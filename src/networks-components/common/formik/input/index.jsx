/* eslint-disable react/jsx-fragments */
import React, { Fragment } from 'react';
import { Input } from '../../default/Input';
import FormikField from '../field';

// type FormikInputProps = {
//   name: string,
//   label?: string,
//   errorText?: string,
//   disabled?: boolean,
//   type?: string,
//   placeholder?: string,
//   margin?: string,
//   view?: string,
// }

const FormikInput = ({
  label,
  errorText,
  disabled,
  type = 'text',
  margin,
  ...otherProps
}) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    label={label}
    margin={margin}
  >
    {({ field, form }) => (
      <Fragment>
        <Input
          {...otherProps}
          {...field}
          disabled={disabled}
          type={type}
          onChange={(e) => {
            let { value } = e.target;
            if (type === 'number') {
              if (value === '') {
                form.setFieldValue(field.name, value);
                return;
              }
              const number = Number(value);
              if (!Number.isNaN(number)) {
                value = number;
              }
            }
            form.setFieldValue(field.name, value);
          }}
        />
      </Fragment>
    )}
  </FormikField>
);

export default FormikInput;

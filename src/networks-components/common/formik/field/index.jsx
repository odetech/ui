import React from 'react';
import { Field } from 'formik';
import { FieldWrapper, ErrorTextStyle, StyledLabel } from './FormikFieldStyles';

// type FormikFieldProps = {
//   field: any,
//   children: ReactNode,
//   errorText?: string,
//   errorPlacement?: string,
//   disabled?: boolean,
//   label?: string,
//   margin?: string,
//   id?: string,
//   wrapperId?: string,
// }

const FormikField = ({
  errorText,
  disabled = false,
  field = null,
  label,
  margin = '0 0 10px 0',
  children,
  id,
  wrapperId,
}) => (
  <FieldWrapper
    disabled={disabled}
    margin={margin}
    htmlFor={id}
    id={wrapperId}
  >
    {label && <StyledLabel>{label}</StyledLabel>}
    <Field {...field}>
      {children}
    </Field>
    {errorText && <ErrorTextStyle>{errorText}</ErrorTextStyle>}
  </FieldWrapper>
);

export default FormikField;

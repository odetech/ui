import styled from '@emotion/styled';
import { transparentize } from 'polished';

export const ErrorTextStyle = styled('div')`
  font-size: 11px;
  color: #F33822;
  white-space: pre-wrap;
  max-width: fit-content;
`;

// type FieldWrapperProps = {
//   disabled: boolean,
//   margin: string,
// };

export const FieldWrapper = styled('label')`
  width: 100%;
  opacity: ${({ disabled }) => (disabled ? '0.6' : '1')};
  align-items: center;
  margin: ${({ margin }) => (margin)};
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};

  &:hover {
    p {
      color: ${transparentize(0.5, '#F7942A')};
    }
  }

  &:active, &:focus {
    p {
      color: #F7942A;
    }
  }

  &:disabled {
    color: #9C9C9C;
  }
`;

export const StyledLabel = styled('p')`
  transition: 0.3s;
  font-size: 14px;
  line-height: 21px;
  margin: 0;
  color: #9C9C9C;
`;

import React from 'react';
import { Input } from '../../default/Input';
import FormikField from '../field';
import { PlusIcon } from '../../icons';
import { Chip, ChipsInputWrapper, InputWrapper } from './FormikChipsInputStyles';

// type FormikChipsInputProps = {
//   name: string,
//   arrayName: string,
//   label?: string,
//   errorText?: string,
//   disabled?: boolean,
//   type?: string,
//   placeholder?: string,
//   margin?: string,
//   view?: string,
//   id?: string,
//   wrapperId?: string,
// }

const FormikChipsInput = ({
  label,
  errorText,
  disabled,
  type = 'text',
  margin,
  arrayName,
  id,
  wrapperId,
  ...otherProps
}) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    label={label}
    margin={margin}
    id={id}
    wrapperId={wrapperId}
  >
    {({ field, form }) => {
      const handleKeyDown = (e) => {
        if (['Enter', 'Tab', ','].includes(e.key)) {
          e.preventDefault();

          const value = field.name.trim();
          if (value && !errorText) {
            form.setFieldValue(field.name, '');
            form.setFieldValue(arrayName, [...form.values[arrayName], field.value]);
          }
        }
      };

      const handleDelete = (item) => {
        const updatedItemsArray = form.values[arrayName].filter((i) => i !== item);
        form.setFieldValue(arrayName, updatedItemsArray);
      };

      return (
        <ChipsInputWrapper>
          {form.values[arrayName].map((item) => (
            <Chip
              key={item}
            >
              <p>
                {item}
              </p>
              <button
                type="button"
                className="button"
                onClick={(e) => {
                  e.preventDefault();
                  handleDelete(item);
                }}
              >
                <PlusIcon />
              </button>
            </Chip>
          ))}
          <InputWrapper>
            <Input
              {...otherProps}
              {...field}
              id={id}
              disabled={disabled}
              type={type}
              onKeyDown={handleKeyDown}
              onChange={(e) => {
                let { value } = e.target;
                if (type === 'number') {
                  if (value === '') {
                    form.setFieldValue(field.name, value);
                    return;
                  }
                  const number = Number(value);
                  if (!Number.isNaN(number)) {
                    value = number;
                  }
                }
                form.setFieldValue(field.name, value);
              }}
            />
          </InputWrapper>
        </ChipsInputWrapper>
      );
    }}
  </FormikField>
);

export default FormikChipsInput;

import styled from '@emotion/styled';

export const ChipsInputWrapper = styled('div')`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  border: 1px solid #BBBBBB;
  padding: 14px 16px 8px 16px;
  border-radius: 7px;

  input {
    border: unset;
    background: transparent;
    padding: 0;
    
    &:hover, &:active, &:focus {
      background: transparent !important;
    }
  }

  button {
    svg {
      transform: rotate(45deg);
    }
  }
`;

export const Chip = styled('div')`
  display: flex;
  align-items: center;
  background: #E3E3E3;
  font-size: 14px;
  line-height: 21px;
  border-radius: 14px;
  padding: 5px 10px;
  margin: 0 10px 5px 0;
  max-width: 250px;

  p {
    margin: 0 5px 0 0;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  
  button {
    border: unset;
    background: transparent;
    padding: 0;
  }
`;

export const InputWrapper = styled('div')`
  width: unset !important;
`;

/* eslint-disable react/jsx-fragments */
import React, { Fragment } from 'react';
import { Textarea } from '../../default/Textarea';
import FormikField from '../field';

// type FormikInputProps = {
//   name: string,
//   label?: string,
//   errorText?: string,
//   disabled?: boolean,
//   placeholder?: string,
//   margin?: string,
//   view?: string,
// }

const Index = ({
  label,
  errorText,
  disabled,
  margin,
  ...otherProps
}) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    label={label}
    margin={margin}
  >
    {({ field, form }) => (
      <Fragment>
        <Textarea
          {...otherProps}
          {...field}
          disabled={disabled}
          onChange={(e) => {
            const { value } = e.target;
            form.setFieldValue(field.name, value);
          }}
        />
      </Fragment>
    )}
  </FormikField>
);

export default Index;

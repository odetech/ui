import React from 'react';
import FormikField from '../field';
import Toggle from '../../default/Toggle';

// type FormikToggleButtonProps = {
//   disabled?: boolean,
//   label?: string,
//   id?: string,
//   name: string,
//   onChangeCallback?: (e: ChangeEvent<HTMLInputElement>) => void,
//   trueValue?: string | null,
//   falseValue?: string | null,
//   margin?: string,
//   errorText?: string,
//   size?: string | 'default' | 'large',
// }

const FormikToggleButton = ({
  disabled = false,
  label = '',
  errorText,
  margin,
  trueValue = null,
  falseValue = null,
  onChangeCallback,
  ...otherProps
}) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    label={label}
    margin={margin}
  >
    {({ field, form }) => (
      <Toggle
        {...otherProps}
        {...field}
        disabled={disabled}
        checked={(trueValue && falseValue) ? field.value === trueValue : !!field.value}
        onChange={(e) => {
          if (field.value === trueValue) {
            form.setFieldValue(field.name, falseValue);
            return;
          }
          if (field.value === falseValue) {
            form.setFieldValue(field.name, trueValue);
            return;
          }
          if (onChangeCallback) {
            onChangeCallback(e);
          }
          field.onChange(e);
        }}
      />
    )}
  </FormikField>
);

export default FormikToggleButton;

import styled from '@emotion/styled';

// type ToggleBlockWrapProps = {
//   margin?: string,
// }

export const ToggleBlockWrap = styled('div')`
  display: flex;
  align-items: center;
  ${(props) => (props.margin && `margin: ${props.margin};`)};
`;

// type ToggleBlockWrapLabelProps = {
//   largeFontSizeLabel?: string,
// }

export const ToggleBlockWrapLabel = styled('p')`
  margin-bottom: 0;
  margin-right: 20px;

  ${(props) => (props.largeFontSizeLabel && `
    font-size: 18px;
    line-heihgt: 22px;
  `)}
`;

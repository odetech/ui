import styled from '@emotion/styled';
import { transparentize } from 'polished';

export const Card = styled('div')`
  background-color: #FFFFFF;
  padding: ${({ padding }) => padding || '20px'};
  border-radius: ${({ borderRadius }) => borderRadius || '15px'};
  box-shadow: 0 0 34px ${transparentize(0.75, '#999999')};

  &:not(:last-child) {
    margin-bottom: 20px;
  }
`;

export const HR = styled('hr')`
  margin: 10px 0;
  height: 1px;
  background: #E5E5E5;
  border: unset;
`;

export const H2 = styled('p')`
  font-weight: 600;
  font-size: 15px;
  margin: 0;
`;

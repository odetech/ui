import styled from '@emotion/styled';
import { transparentize } from 'polished';
import { defaultTheme } from '../../../../palette';

// type ToggleWrapperProps = {
//   margin?: string;
//   size?: string | 'default' | 'large',
// };

export const ToggleWrapper = styled('label')`
  width: 100%;
  margin: ${({ margin }) => (margin)};
  display: flex;
  justify-content: space-between;
  margin-bottom: 0;
  cursor: pointer;

  input {
    position: absolute;
    z-index: -1;
    opacity: 0;
  }

  & input:checked + div::after {
    left: ${({ size }) => (size === 'large' ? '18px' : '15px')};
    background: ${defaultTheme.accent};
  }

  & input:checked + div::before {
    background: ${transparentize(0.8, defaultTheme.accent)};
    border-color: ${defaultTheme.accent};
  }

  & input:disabled + div,
  & input:checked:disabled + div {
    opacity: 0.6;
  }

  &:focus {
    span {
      color: ${transparentize(0.5, defaultTheme.accent)};
    }
  }

  &:active, &:hover {
    span {
      color: ${defaultTheme.accent};
    }
  }

  &:disabled {
    span {
      color: ${defaultTheme.label};
    }
  }
`;

// type MainToggleElementProps = {
//   disabled?: boolean,
//   size?: string | 'default' | 'large',
// }

export const MainToggleElement = styled('div')`
  position: relative;
  height: 100%;
  width: ${({ size }) => (size === 'large' ? '38px' : '30px')};
  cursor: ${({ disabled }) => (disabled ? 'unset' : 'pointer')};

  &::before {
    content: '';
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: 0;
    width: ${({ size }) => (size === 'large' ? '38px' : '30px')};
    height: ${({ size }) => (size === 'large' ? '22px' : '16px')};
    border-radius: 50px;
    border: 0.5px solid ${defaultTheme.label};
    background-color: transparent;
    transition: .2s;
  }

  &::after {
    content: '';
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: ${({ size }) => (size === 'large' ? '4px' : '2px')};
    width: ${({ size }) => (size === 'large' ? '16px' : '12px')};
    height: ${({ size }) => (size === 'large' ? '16px' : '12px')};
    border-radius: 10px;
    background-color: ${defaultTheme.label};
    transition: .2s;
  }
`;

export const ToggleLabel = styled('span')`
  transition: 0.3s;
  font-size: 14px;
  line-height: 21px;
  margin: 0;
  color: ${defaultTheme.label};
  display: flex;
  align-items: center;
`;

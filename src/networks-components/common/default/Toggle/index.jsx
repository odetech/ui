import React from 'react';
import {
  ToggleWrapper, ToggleLabel, MainToggleElement,
} from './ToggleStyles';

// type ToggleProps = {
//   id?: string,
//   name?: string,
//   checked: boolean,
//   onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
//   disabled?: boolean,
//   label?: string | ReactNode,
//   wrapperId?: string,
//   size?: string | 'default' | 'large',
// };

const Toggle = ({
  id,
  checked,
  onChange,
  disabled,
  name,
  label,
  wrapperId = '',
  size = 'default',
  ...props
}) => (
  <ToggleWrapper htmlFor={id} id={wrapperId} size={size}>
    {label && <ToggleLabel>{label}</ToggleLabel>}
    <input
      id={id}
      type="checkbox"
      name={name}
      onChange={onChange}
      checked={checked}
      disabled={disabled}
      {...props}
    />
    <MainToggleElement disabled={disabled} size={size} />
  </ToggleWrapper>
);

export default Toggle;

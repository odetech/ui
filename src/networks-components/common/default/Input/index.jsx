import React from 'react';
import { InputWrapper, StyledInput, StyledLabel } from './InputStyles';

// export type InputProps = {
//   label?: string;
//   disabled?: boolean;
//   type?: string;
//   name: string;
//   id?: string;
//   value?: string;
//   placeholder?: string;
//   margin?: string;
//   view?: 'open' | 'closed' | string;
//   onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
//   onKeyDown?: (e: KeyboardEvent) => void;
// };

export const Input = ({
  // Default props
  disabled = false,
  label,
  view = 'closed',
  margin = '0 0 0 0',
  type = 'text',
  ...props
}) => (
  <InputWrapper
    margin={margin}
  >
    {label && <StyledLabel>{label}</StyledLabel>}
    <StyledInput
      view={view}
      disabled={disabled}
      type={type}
      {...props}
    />
  </InputWrapper>
);

export default Input;

import React from 'react';
import { StyledButton } from './ButtonStyles';

// export type ButtonProps = {
//   id?: string;
//   onClick?: (e: MouseEvent<HTMLButtonElement>) => void;
//   disabled?: boolean;
//   type?: 'button' | 'reset' | 'submit';
//   children: ReactNode;
//   size?: 'lg' | 'sm' | 'md';
//   view?: 'default' | 'text';
//   color?: string | 'orange' | 'green' | 'red' | 'pink' | 'yellow' | 'blue' | 'purple' | 'black';
// };

const chooseColor = (color) => {
  let buttonColor;

  switch (color) {
    case 'orange': {
      buttonColor = '#F7942A';
      break;
    }
    case 'green': {
      buttonColor = '#4BC133';
      break;
    }
    case 'red': {
      buttonColor = '#F33822';
      break;
    }
    case 'pink': {
      buttonColor = '#E54367';
      break;
    }
    case 'yellow': {
      buttonColor = '#FFA800';
      break;
    }
    case 'blue': {
      buttonColor = '#3B7EE1';
      break;
    }
    case 'purple': {
      buttonColor = '#9131DD';
      break;
    }
    case 'black': {
      buttonColor = '#3B3C3B';
      break;
    }

    default: {
      buttonColor = '#F7942A';
    }
  }

  return buttonColor;
};

export const Button = ({
  // Default props
  type = 'button',
  size = 'md',
  view = 'default',
  disabled = false,
  color = 'orange',
  children,
  ...props
}) => (
  <StyledButton
    type={type}
    disabled={disabled}
    size={size}
    view={view}
    buttonColor={chooseColor(color)}
    {...props}
  >
    {children}
  </StyledButton>
);

export default Button;

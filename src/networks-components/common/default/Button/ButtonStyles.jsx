import styled from '@emotion/styled';
import { transparentize, darken } from 'polished';

// type ButtonProps = {
//   disabled?: boolean;
//   size?: 'lg' | 'sm' | 'md';
//   view?: 'default' | 'text';
//   buttonColor: string;
// };

export const StyledButton = styled('button')`
  font-size: 16px;
  transition: 0.3s;
  cursor: pointer;
  outline: none;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ view, buttonColor }) => (view === 'default' ? buttonColor : 'transparent')} none;
  border: none;
  border-radius: 7px;
  color: ${({ view, buttonColor }) => (view === 'default' ? '#FFFFFF' : buttonColor)};
  font-weight: 500;

  // Size of the Button (small(sm), large(lg), medium(default/md))
  ${({ size }) => {
    switch (size) {
      case 'sm':
        return {
          padding: '4px 20px',
          maxHeight: '32px',
        };
      case 'lg':
        return {
          padding: '12px 32px',
          maxHeight: '48px',
        };
      default:
        return {
          padding: '8px 24px',
          maxHeight: '40px',
        };
    }
  }};

  &:focus {
    outline: none;
    background: ${({ view, buttonColor }) => (view === 'default'
    ? darken(0.15, buttonColor) : '#FDDFC0')};
  }

  &:hover,
  &:active {
    background: ${({ view, buttonColor }) => (view === 'default'
    ? transparentize(0.25, buttonColor) : '#FFF5EA')};
  }

  &:disabled {
    cursor: not-allowed;
    background: #F0F0F0;
    color: #9C9C9C;

    svg {
    }
  }
`;

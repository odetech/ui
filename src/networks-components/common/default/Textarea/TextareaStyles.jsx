import styled from '@emotion/styled';
import { transparentize } from 'polished';

// type TextareaProps = {
//   view: 'open' | 'closed' | string;
// }

export const StyledTextarea = styled('textarea')`
  resize: none;
  position: relative;
  font-weight: 500;
  font-size: 17px;
  line-height: inherit;
  padding: 8px 16px;
  width: 100%;
  min-width: 200px;
  min-height: 213px;
  transition: 0.3s;
  text-overflow: ellipsis;
  color: #3B3C3B;
  border-radius: 7px;

  background: transparent;
  border: ${({ view }) => (view === 'closed' ? '1px solid #BBBBBB' : 'none')};
  border-bottom: ${({ view }) => (view === 'open' && '1px solid #BBBBBB')};

  &::placeholder {
    color: #9C9C9C;
  }

  &:focus {
    box-shadow: none;
    outline: none;
  }

  &:disabled {
    color: #9C9C9C;
    border-color: #E5E5E5 !important;
    background-color: #F0F0F0 !important;
    cursor: not-allowed;
  }

  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus {
    border-color: #F7942A;
    -webkit-text-fill-color: #3B3C3B;
    -webkit-box-shadow: 0 0 0 1000px ${transparentize(0.6, '#F7942A')} inset;
    transition: background-color 5000s ease-in-out 0s;
    font-weight: 500;
    font-size: 17px;
  }

  &::-webkit-scrollbar-track {
    border-radius: 7px;
    width: 6px;
    background-color: #F0F0F0;
  }

  &::-webkit-scrollbar {
    border-radius: 7px;
    width: 6px;
    background-color: #F0F0F0;
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 7px;
    width: 6px;
    background-color: ${transparentize(0.5, '#9C9C9C')};
  }
`;

export const StyledLabel = styled('p')`
  transition: 0.3s;
  font-size: 14px;
  line-height: 21px;
  margin: 0;
  color: #9C9C9C;
`;

// type TextareaWrapperProps = {
//   margin?: string;
// };

export const TextareaWrapper = styled('div')`
  width: 100%;
  margin: ${({ margin }) => (margin)};

  &:focus {
    p {
      color: ${transparentize(0.5, '#F7942A')};
    }

    input {
      border-color: ${transparentize(0.5, '#F7942A')};
    }
  }

  &:active, &:hover {
    p {
      color: #F7942A;
    }

    input {
      border-color: #F7942A;
      background-color: ${transparentize(0.9, '#F7942A')};
    }
  }

  &:disabled {
    p {
      color: #9C9C9C;
    }

    input {
      border-color: #E5E5E5;
      background-color: #F0F0F0;
    }
  }
`;

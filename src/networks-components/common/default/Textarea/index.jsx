import React from 'react';
import { TextareaWrapper, StyledTextarea, StyledLabel } from './TextareaStyles';

// export type TextareaProps = {
//   label?: string;
//   disabled?: boolean;
//   name: string;
//   id?: string;
//   value?: string;
//   placeholder?: string;
//   margin?: string;
//   view?: 'open' | 'closed' | string;
//   onChange?: (e: ChangeEvent<HTMLTextAreaElement>) => void;
// };

export const Textarea = ({
  // Default props
  disabled = false,
  label,
  view = 'closed',
  margin = '0 0 0 0',
  ...props
}) => (
  <TextareaWrapper
    margin={margin}
  >
    {label && <StyledLabel>{label}</StyledLabel>}
    <StyledTextarea
      view={view}
      disabled={disabled}
      {...props}
    />
  </TextareaWrapper>
);

export default Textarea;

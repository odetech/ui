import React from 'react';
import Modal from 'react-modal';
import { StyledModal, CloseButton } from './ModalStyles';
import { PlusIcon } from '../../icons';

Modal.setAppElement('#root');

// export type ModalProps = {
//   onClose: () => void;
//   isOpen: boolean;
//   children: ReactNode;
//   overlayStyles?: CSSProperties;
//   contentStyles?: CSSProperties;
// };

export const CustomModal = ({
  // Default props
  onClose,
  children,
  overlayStyles,
  contentStyles,
  ...props
}) => (
  <StyledModal
    {...props}
    style={{
      overlay: {
        zIndex: 1000,
        ...overlayStyles,
      },
      content: {
        maxWidth: 450,
        maxHeight: 600,
        ...contentStyles,
      },
    }}
  >
    <CloseButton
      type="button"
      onClick={onClose}
    >
      <PlusIcon />
    </CloseButton>
    {children}
  </StyledModal>
);

export default CustomModal;

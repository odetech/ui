import styled from '@emotion/styled';
import Modal from 'react-modal';
import { transparentize } from 'polished';

export const StyledModal = styled(Modal)`
  font-family: 'Poppins', sans-serif;
  position: absolute;
  margin: auto;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: #FFFFFF;
  border-radius: 15px;
  box-shadow: 0 0 20px ${transparentize(0.75, '#999999')};
  padding: 30px;

  width: 100%;
  height: 100%;

  display: flex;
  flex-direction: column;
  
  &:focus {
    outline: none;
  }

  &::-webkit-scrollbar-track {
    border-radius: 2px;
    width: 6px;
    background-color: #F0F0F0;
    margin: 10px 0;
  }

  &::-webkit-scrollbar {
    border-radius: 2px;
    width: 6px;
    background-color: transparent;
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 2px;
    width: 6px;
    background-color: ${transparentize(0.5, '#9C9C9C')};
  }
`;

export const CloseButton = styled('button')`
  background: none;
  border: none;
  position: absolute;
  right: 10px;
  top: 10px;

  svg {
    transform: rotate(45deg);
  }
`;

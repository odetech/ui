import React from 'react';
import styled from '@emotion/styled';

// type IconsProps = {
//   width?: string;
//   height?: string;
//   fill?: string;
//   stroke?: string;
//   viewBox?: string;
// };

const StyledIcon = styled('svg')`
  width: ${({ width }) => width || '20px'};
  height: ${({ height }) => height || '20px'};
  stroke: ${({ stroke }) => stroke};
  fill: ${({ fill }) => fill};
  stroke-width: ${({ strokeWidth }) => strokeWidth};
  transition: 0.3s;
`;

export const ChevronLeftIcon = ({ viewBox, ...props }) => (
  <StyledIcon viewBox={viewBox || '0 0 24 24'} {...props}>
    <path
      d="M15.41,16.58L10.83,12L15.41,7.41L14,6L8,12L14,18L15.41,16.58Z"
    />
  </StyledIcon>
);

export const ChevronRightIcon = ({ viewBox, ...props }) => (
  <StyledIcon viewBox={viewBox || '0 0 24 24'} {...props}>
    <path
      d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z"
    />
  </StyledIcon>
);

export const PlusIcon = ({ viewBox, ...props }) => (
  <StyledIcon viewBox={viewBox || '0 0 24 24'} {...props}>
    <path
      d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"
    />
  </StyledIcon>
);

/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from 'react';
import { BeatLoader } from 'react-spinners';
import { Form, Formik } from 'formik';
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';
import { ChevronLeftIcon, ChevronRightIcon } from '../common/icons';
import {
  HeaderWrapper, StyledCard, UsersWrapper, StyledButton, StyledInput,
  PaginationWrapper, SpinnerWrapper, NetworksHR,
} from '../NetworkStyles';
import { H2 } from '../common/BasicStyledComponents';
import UserProfile from '../user-profile';

const USERS_AMOUNT = 9;

// type UserDataProps = {
//   createdAt: string,
//   emails: { address: string, verified: boolean }[],
//   profile: {
//     aboutMe: string,
//     avatar: null | {
//       secureUrl: string,
//       _id: string,
//     },
//     createdBy: string,
//     createdFrom: string,
//     firstName: string,
//     lastName: string,
//     onboardings: null,
//     roles: {
//       _id: string,
//       isStaff: boolean,
//     } | null,
//     skillsets: null,
//     tagline: string,
//   },
//   username: string,
//   _id: string,
// };

// type NetworksProps = {
//   loading: boolean,
//   networksData: {
//     createdAt: string,
//     isPending: boolean,
//     updatedAt: string,
//     userId1: string,
//     userId2: string,
//     _id: string,
//     user2: UserDataProps,
//   }[] | null | undefined,
//   networksAmount: number,
//   validation: (values: unknown) => void,
//   fetchNetworks: (currentPage: number) => void,
//   onSubmit: (email: string) => void,
//   onUserClick: (isProfileCardOpen: UserDataProps | null) => void,
// };

export const Networks = ({
  onUserClick, validation, networksAmount, networksData, loading, fetchNetworks, onSubmit, cardPadding,
  cardBorderRadius, userButtonMargin, usersWrapperMargin,
}) => {
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    fetchNetworks(currentPage);
  }, [currentPage]);

  const totalPages = Math.ceil((networksAmount) / USERS_AMOUNT);

  return (
    <StyledCard
      padding={cardPadding}
      borderRadius={cardBorderRadius}
    >
      <HeaderWrapper>
        <H2>
          {`Network: ${networksAmount}`}
        </H2>
        {/* TODO: Add menu button */}
      </HeaderWrapper>
      {!loading && networksData ? (
        <UsersWrapper
          margin={usersWrapperMargin}
        >
          {networksData.map((network) => (
            <UserProfile
              key={network.user2 && network.user2._id}
              user={network.user2}
              onUserClick={onUserClick}
              userButtonMargin={userButtonMargin}
            />
          ))}
        </UsersWrapper>
      )
        : <SpinnerWrapper><BeatLoader color="#F7942A" /></SpinnerWrapper>}
      <PaginationWrapper
        disabled={`${loading || totalPages === 1}`}
      >
        <Pagination
          onChange={(page) => {
            setCurrentPage(page);
          }}
          total={networksAmount}
          pageSize={USERS_AMOUNT}
          prevIcon={<ChevronLeftIcon />}
          nextIcon={<ChevronRightIcon />}
          showTitle={false}
          showLessItems
          defaultCurrent={1}
        />
      </PaginationWrapper>
      <NetworksHR />
      <Formik
        initialValues={{ email: '' }}
        onSubmit={(values) => {
          onSubmit(values.email);
        }}
        validate={validation}
      >
        {({
          errors,
        }) => (
          <Form>
            <StyledInput
              name="email"
              placeholder="E-mail"
              errorText={errors.email}
            />
            <StyledButton
              type="submit"
              disabled={loading}
              color="blue"
            >
              Invite
            </StyledButton>
          </Form>
        )}
      </Formik>
    </StyledCard>
  );
};

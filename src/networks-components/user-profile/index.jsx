import React from 'react';
import { Avatar } from '../../avatar';
import {
  Name, UserPreview, InvisibleProfilePreviewButton,
} from '../NetworkStyles';

// type UserProfileProps = {
//   user: UserDataProps,
//   onUserClick: (isProfileCardOpen: UserDataProps | null) => void,
// };

const UserProfile = ({
  user, onUserClick, userButtonMargin,
}) => (
  <InvisibleProfilePreviewButton
    onClick={() => {
      onUserClick(user);
    }}
  >
    <UserPreview
      margin={userButtonMargin}
    >
      <Avatar user={user} size={60} isSquare />
      <Name>
        {(user && user.profile && user.profile.firstName) || ''}
      </Name>
    </UserPreview>
  </InvisibleProfilePreviewButton>
);

export default UserProfile;

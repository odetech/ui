/* eslint-disable react/jsx-fragments */
import React, { Fragment, useState } from 'react';
import { Form, Formik } from 'formik';
import { transparentize } from 'polished';
import Modal from '../common/default/Modal';
import FormikInput from '../common/formik/input';
import FormikTextarea from '../common/formik/textarea';
import FormikToggleButton from '../common/formik/toggle-button';
import FormikChipsInput from '../common/formik/chips-input';
import {
  ToggleWrapper,
  InvitationInputWrapper,
  InvitationTitle,
  InvitationHR,
  InvitationSubmitButton,
  InvitationCompleteImg,
  InvitationCompleteTitle,
  InvitationCompleteButtonsWrapper,
  InvitationCompleteCloseButton,
  InvitationCompleteWrapper,
  InvitationCompleteSendMoreButton,
} from '../NetworkStyles';
import InvitationSentImgPath from './img/InvitationsMessage.svg';

// type InvitationModalProps = {
//   defaultInviteEmail: string | null;
//   userFirstName: string;
//   showModal: boolean;
//   setShowModal: (showModal: boolean) => void;
//   onSubmit: (payloadData: {
//     emails: string[],
//     subject: string,
//     message: string,
//   }) => void;
//   validation: (values: unknown) => void,
// }

export const InvitationModal = ({
  showModal, setShowModal, onSubmit, userFirstName, defaultInviteEmail, validation,
}) => {
  const [isInvitationsSent, setIsInvitationsSent] = useState(false);
  const [notFirstSubmit, setNotFirstSubmit] = useState(false);

  return (
    <Modal
      isOpen={showModal}
      onClose={() => setShowModal(false)}
      contentStyles={{
        maxWidth: 618,
        maxHeight: isInvitationsSent ? 480 : 624,
        overflow: 'auto',
      }}
      overlayStyles={{
        background: `${transparentize(0.3, '#000000')}`,
      }}
    >
      {!isInvitationsSent ? (
        <Fragment>
          <InvitationTitle>
            Grow Your Network
          </InvitationTitle>
          <InvitationHR />
          <Formik
            initialValues={{
              customMessage: false,
              emails: (defaultInviteEmail && !notFirstSubmit) ? [defaultInviteEmail] : [],
              emailsInput: '',
              subject: 'Join my OdeCloud network!',
              // eslint-disable-next-line max-len
              message: `Hi There,\n\n\nI recently joined OdeCloud, a platform with a growing and collaborative community of NetSuite experts. I’d like to invite you to join my network.\n\nHope to see you there soon.\n\n\n${userFirstName}`,
            }}
            onSubmit={(values) => {
              onSubmit(values);
              setIsInvitationsSent(true);
              setNotFirstSubmit(true);
            }}
            validate={validation}
          >
            {({
              errors, values,
            }) => (
              <Form>
                <InvitationInputWrapper>
                  <FormikChipsInput
                    name="emailsInput"
                    arrayName="emails"
                    placeholder="E-mails"
                    label="Email (press enter for multiple)"
                    // @ts-ignore
                    errorText={(values.emailsInput === '' && errors.emails) || errors.emailsInput}
                    margin="0 0 20px 0"
                    id="emailsInput"
                    wrapperId="emails"
                  />
                </InvitationInputWrapper>
                <ToggleWrapper>
                  <p>
                    Want To Customize Message?
                  </p>
                  <FormikToggleButton
                    name="customMessage"
                    errorText={errors.customMessage}
                    margin="0"
                  />
                </ToggleWrapper>
                <InvitationInputWrapper>
                  <FormikInput
                    name="subject"
                    placeholder="E-mail theme"
                    errorText={errors.subject}
                    margin="0 0 20px 0"
                    disabled={!values.customMessage}
                  />
                </InvitationInputWrapper>
                <FormikTextarea
                  name="message"
                  placeholder="E-mail message"
                  errorText={errors.message}
                  disabled={!values.customMessage}
                  margin="0 0 24px 0"
                />
                <InvitationSubmitButton
                  type="submit"
                >
                  Send Invitation
                </InvitationSubmitButton>
              </Form>
            )}
          </Formik>
        </Fragment>
      ) : (
        <InvitationCompleteWrapper>
          <InvitationCompleteImg
            alt="InvitationSentImg"
            src={InvitationSentImgPath}
          />
          <InvitationCompleteTitle>
            Invitations sent successfully
          </InvitationCompleteTitle>
          <InvitationCompleteButtonsWrapper>
            <InvitationCompleteCloseButton
              view="text"
              onClick={() => {
                setShowModal(false);
              }}
            >
              Close
            </InvitationCompleteCloseButton>
            <InvitationCompleteSendMoreButton
              onClick={() => {
                setIsInvitationsSent(false);
              }}
            >
              Send More
            </InvitationCompleteSendMoreButton>
          </InvitationCompleteButtonsWrapper>
        </InvitationCompleteWrapper>
      )}
    </Modal>
  );
};

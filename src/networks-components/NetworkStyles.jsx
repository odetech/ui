import styled from '@emotion/styled';
import { transparentize } from 'polished';
import { Card, HR } from './common/BasicStyledComponents';
import { Button } from './common/default/Button';
import FormikInput from './common/formik/input';

export const InvisibleProfilePreviewButton = styled(Button)`
  max-height: unset;
  height: fit-content;
  padding: 0;
  background: transparent;

  &:hover, &:active, &:focus {
    background: transparent;
  }
`;

export const HeaderWrapper = styled('div')`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 0 0 10px 0;
`;

export const StyledCard = styled(Card)`
  min-width: 250px;
`;

export const Name = styled('p')`
  font-weight: 600;
  font-size: 10px;
  margin: 0 0 0 4px;
  max-width: 50px;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const UserPreview = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: ${({ margin }) => (margin || '0 15px 10px 0')};
  cursor: pointer;
  color: #3B3C3B;

  &:hover {
    color: ${transparentize(0.2, '#F7942A')};
    text-decoration: none;
  }
`;

export const UsersWrapper = styled('div')`
  display: flex;
  flex-wrap: wrap;
  margin: ${({ margin }) => (margin || '0 -15px 0 0')};

  .btn {
    padding: 0 !important;
  }
`;

export const StyledInput = styled(FormikInput)`
  border: none;
  padding: 7px 10px;
  background: #F0F0F0;
  border-radius: 6px;
  font-size: 12px;
  font-weight: 500;
  max-width: unset;
`;

export const StyledButton = styled(Button)`
  width: 100%;
  font-weight: 500;
  font-size: 15px;
  line-height: 22px;
`;

export const StyledPageButton = styled(Button)`
  padding: 2px 4px;
  min-width: 14px;
  font-weight: 600;
  font-size: 10px;
  border-radius: 2px;

  background-color: ${({ active }) => (active === 'true' ? transparentize(0.7, '#F7942A') : '#F0F0F0')};
  color: ${({ active }) => (active === 'true' ? '#F7942A' : '#3B3C3B')};

  &:focus {
    background-color: ${transparentize(0.7, '#F7942A')};
    color: #F7942A;
  }

  &:hover,
  &:active {
    background-color: #F7942A;
    color: #FFFFFF;
  }

  &:disabled {
    background-color: #F0F0F0;
    color: #9C9C9C;
  }
`;

// type PaginationWrapperProps = {
//   disabled?: string,
// }

export const PaginationWrapper = styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;

  .rc-pagination {

    .rc-pagination-prev,
    .rc-pagination-next,
    .rc-pagination-item,
    .rc-pagination-jump-next,
    .rc-pagination-jump-prev {
      height: 28px;
      width: 20px;
      min-width: 16px;
      padding: 0;
      margin: 0 4px;
    }

    .rc-pagination-jump-next,
    .rc-pagination-jump-prev {

      button {
        padding: 0;
      }
    }

    .rc-pagination-options {
      display: none;
    }

    .rc-pagination-prev,
    .rc-pagination-next {
      display: none;
      background: #FFF5EA;

      svg {
        fill: #F7942A;
      }
    }

    .rc-pagination-disabled {
      background: #F0F0F0;

      svg {
        fill: #9C9C9C;
      }
    }

    .rc-pagination-item {
      border: unset;
      font-weight: bold;
      font-size: 12px;
      transition: 0.3s;

      a {
        transition: 0.3s;
        padding: 0;
      }

      &:hover, &:focus, &:active {
        border: unset;
        color: #F7942A !important;

        a {
          color: #F7942A !important;
        }
      }
    }

    .rc-pagination-item-active {
      background: #FDDFC0;
      color: #F7942A;
    }
  }

  ${({ disabled }) => (disabled === 'true' && `
    cursor: not-allowed;
    pointer-events: none;

    .rc-pagination {
      .rc-pagination-prev {
        background: #F0F0F0;

        svg {
          fill: #9C9C9C;
        }
      }
      .rc-pagination-next {
        background: #F0F0F0;

        svg {
          fill: #9C9C9C;
        }
      }
      .rc-pagination-item {
        background: #F0F0F0;

        a {
          color: #9C9C9C !important;
        }
      }
      .rc-pagination-item-active {
        background: #F0F0F0;

        a {
          color: #9C9C9C !important;
        }
      }
    }
  `)}
`;

export const SpinnerWrapper = styled('div')`
  min-height: 286px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ToggleWrapper = styled('label')`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 0 0 5px 0;
  cursor: pointer;

  p {
    margin: 0;
    font-weight: 500;
    font-size: 12px;
    line-height: 18px;
    width: 100%;
  }

  label {
    width: unset;
  }
`;

export const InvitationInputWrapper = styled('div')`
  input {
    max-width: unset;
    border-radius: 3px;
  }

  p {
    color: #3B3C3B !important;
    font-weight: 500 !important;
    font-size: 12px !important;
    line-height: 18px;
    margin: 0 0 5px 0;
  }
`;

export const InvitationTitle = styled('p')`
  font-weight: 500;
  font-size: 25px;
  line-height: 37px;
  margin: 0;
`;

export const InvitationHR = styled(HR)`
  margin: 20px -30px;
`;

export const InvitationSubmitButton = styled(Button)`
  padding: 15px 40px;
  font-weight: 500;
  font-size: 18px;
  line-height: 27px;
`;

export const InvitationCompleteWrapper = styled('div')`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const InvitationCompleteImg = styled('img')`
  margin: 0 0 28px 0;
  width: 178px;
`;

export const InvitationCompleteTitle = styled('span')`
  font-weight: 500;
  font-size: 25px;
  line-height: 37px;
  margin: 0 0 50px 0;
`;

export const InvitationCompleteButtonsWrapper = styled('div')`
  display: flex;
`;

export const InvitationCompleteCloseButton = styled(Button)`
  border: 2px solid #F7942A;
  margin: 0 20px 0 0;
  padding: 15px 40px;
  max-height: unset;
  font-weight: 500;
  font-size: 18px;
  line-height: 27px;
`;

export const InvitationCompleteSendMoreButton = styled(Button)`
  padding: 15px 40px;
  max-height: unset;
  font-weight: 500;
  font-size: 18px;
  line-height: 27px;
`;

export const NetworksHR = styled(HR)`
  color: #F0F0F0;
`;

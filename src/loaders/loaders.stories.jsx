import React from 'react';
import { LoadingSmallDots } from './loading-small-dots/loading-small-dots';
import { LoadingComponent } from './loading/loading.component';

export default {
  component: LoadingComponent,
  title: 'Loaders',
}

export const SmallDots = () => <LoadingSmallDots />;

export const Infinity = () => <LoadingComponent />;

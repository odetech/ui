import React from "react";
import { BeatLoader } from "react-spinners";
import { css } from "@emotion/react";
import {defaultTheme} from "../../palette";

const override = css`
  display: block;
  text-align: center;
`;

export const LoadingSmallDots = () => (
  <BeatLoader color={defaultTheme.accent} loading size={10} css={override} />
);

import React, { useState } from 'react';
import { EditorState } from 'draft-js';
import { WysiwygEditorFormikComponent } from './formik-wysiwyg-editor.component';

const connections = '{"_id":"eTAspwGsbcWdC6ciF","networks":[{"_id":"FyvaRdDCFgwf7kTRw","profile":{"firstName":"Osar","lastName":"Iyamu"}},{"_id":"SG8aYLxtAK7cbpakm","profile":{"firstName":"Vanielle","lastName":"Lee"}},{"_id":"Wj2PKhcSacD3YiHaP","profile":{"firstName":"Ivan","lastName":"Ivanov"}},{"_id":"dGB3KgwYDv6mCsNrT","profile":{"firstName":"Ravil","lastName":"Nazyrov"}},{"_id":"w4uk84PxC4htsotvq","profile":{"firstName":"Timur","lastName":"Akmatov"}}]}'

const getUserSuggestions = (networksConnections) => {
  if (!networksConnections) {
    return {};
  }
  const result = {};
  networksConnections.forEach((user) => {
    result[user._id] = {
      profile: user.profile,
    };
  });

  return result;
};


const Editor = () => {
  const [newMessage, setNewMessage] = useState(() => EditorState.createEmpty());
  const isEdit = false;
  const values = {
    message: ''
  }

  return (
    <WysiwygEditorFormikComponent
      editorState={newMessage}
      onEditorStateChange={setNewMessage}
      placeholder="What’s on your mind?"
      setFieldValue={() => {}}
      value={values.message}
      field="message"
      handleSubmit={() => {}}
      buttonCallback={() => {}}
      userSuggestions={getUserSuggestions(JSON.parse(connections).networks)}
      isChannelMention
      isEdit={isEdit}
      close={() => (setNewMessage(() => EditorState.createEmpty()))}
      setEditorEmpty={() => ({})}
      setIsReply={() => ({})}
      uploadImageCallBack={async (file) => {
        // const payload = odecloud.articles.payload.getPostPayload();
        // payload.content = ' ';
        // payload.rawContent = ' ';
        // payload.isDraft = true;
        // payload.createdBy = userData?.info?._id;
        // const result = await odecloud.articles.post(payload);
        // const { _id } = result || {};
        //
        // return uploadImageCallback(
        //   file, _id, userData?.info?._id, 'articles',
        // );
      }}
      minOptions
      removeExtraOptions
      disabled={values.message === '' || values.message === '<p></p>\n'}
    />
  )
}

export default {
  component: Editor,
  title: 'Editor',
}

export const DraftJs = () => <Editor />;

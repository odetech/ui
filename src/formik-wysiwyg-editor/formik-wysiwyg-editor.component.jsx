import React, { useEffect, useState } from 'react';
import draftToHtml from 'draftjs-to-html';
import { convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import './formik-wysiwyg-editor.component.scss';
import {
  initializeEditorValue,
  onEnterHandler,
  resetEditorState,
  updateEditorValue
} from './formik-wysiwyg-editor.helpers';
import { CloseIcon, EditIcon } from './formik-wysiwyg-editor.icons.jsx';

import BoldIcon from './icons/bold.svg';
import ItalicIcon from './icons/italic.svg';
import UnderlineIcon from './icons/underline.svg';
import ListIcon from './icons/list.svg';
import OrderedListIcon from './icons/orderedList.svg';
import LinkIcon from './icons/link.svg';
import UnlinkListIcon from './icons/unlink.svg';
import AttachmentIcon from './icons/attachment.svg';

const SubmitEditor = ({ buttonCallback, resetEditorState, disabled }) => (
  <button
    type="button"
    onClick={() => {
      buttonCallback();
      resetEditorState();
    }}
    style={{
      padding: '0 20px', marginLeft: 'auto', opacity: disabled ? '0.3' : '1', cursor: disabled ? 'not-allowed' : 'pointer'
    }}
    disabled={disabled}
  >
    <svg width="21" height="18" viewBox="0 0 21 18" fill="#000">
      <path fillRule="evenodd" clipRule="evenodd" d="M2.01 3.03L9.52 6.25L2 5.25L2.01 3.03ZM9.51 11.75L2 14.97V12.75L9.51 11.75ZM0.01 0L0 7L15 9L0 11L0.01 18L21 9L0.01 0Z" />
    </svg>
  </button>
);

// TODO: add propTypes
export const WysiwygEditorFormikComponent = ({
  setFieldValue,
  handleSubmit,
  userSuggestions,
  buttonCallback,
  field,
  uploadImageCallBack,
  value,
  isEdit,
  isChannelMention,
  replyComment,
  close,
  setEditorEmpty,
  setIsReply,
  removeExtraOptions,
  minOptions,
  isSubmitting,
  disabled,
  error,
  previewImage = true,
  customBlockRendererFn,
  imageComponent,
  isTextUpdate,
  setTextUpdate,
}) => {
  const [editorState, setEditorState] = useState(() => initializeEditorValue(value));
  const [showExtraToolbar, setShowExtraToolbar] = useState(false);
  const isDisabled = disabled || error;

  useEffect(() => {
    if ((isEdit || replyComment) && !isSubmitting) {
      setEditorState(updateEditorValue(value));
    }
  }, [isEdit, replyComment]);

  useEffect(() => {
    if (!editorState.getCurrentContent().hasText() && value && !isSubmitting && isTextUpdate) {
      setEditorState(updateEditorValue(value));
      setTextUpdate(false);
    }
  }, [value, isTextUpdate]);

  useEffect(() => {
    if (editorState.getCurrentContent().hasText()) {
      setEditorEmpty(true);
      return;
    }
    setEditorEmpty(false);
  }, [editorState]);

  const suggestions = Object.entries(userSuggestions).map(([key, value]) => ({
    text: `${value?.profile.firstName} ${value?.profile.lastName}`,
    value: `${value?.profile.firstName} ${value?.profile.lastName}`,
    url: `/?mention=@${key}`,
  }));

  if (isChannelMention) {
    suggestions.splice(0, 0, {
      text: 'channel',
      value: 'channel',
      url: '/?mention=@channel',
    });
  }

  return (
    <div className="wysiwyg-formik">
      {isEdit && (
        <div className="wysiwyg-formik__edit-wrapper">
          <span>{replyComment ? 'Reply' : 'Edit'}</span>
          <button
            className="wysiwyg-formik__edit-close"
            onClick={() => { close(); resetEditorState(setEditorState); }}
          >
            <CloseIcon />
          </button>
        </div>
      )}
      <Editor
        editorState={editorState}
        editorClassName="wysiwyg-formik__editor"
        wrapperClassName="wysiwyg-formik__wrapper"
        toolbarClassName={`wysiwyg-formik__toolbar ${showExtraToolbar ? 'wysiwyg-formik__toolbar_extra-options' : ''} ${minOptions ? 'wysiwyg-formik__toolbar_min-options' : ''}`}
        onEditorStateChange={(value) => {
          const currentContent = draftToHtml(convertToRaw(value.getCurrentContent()));
          setFieldValue(field, currentContent);
          setEditorState(value);
        }}
        keyBindingFn={(e) => onEnterHandler(e, handleSubmit, setEditorState, setIsReply, isDisabled)}
        spellCheck
        toolbar={{
          options: [
            'inline',
            'blockType',
            'fontSize',
            'fontFamily',
            'list',
            'textAlign',
            'colorPicker',
            'link',
            'emoji',
            'image',
          ],
          inline: {
            bold: { icon: BoldIcon },
            italic: { icon: ItalicIcon },
            underline: { icon: UnderlineIcon },
          },
          list: {
            inDropdown: true,
            unordered: {
              icon: ListIcon,
            },
            ordered: {
              icon: OrderedListIcon,
            },
          },
          link: {
            link: {
              icon: LinkIcon,
            },
            unlink: {
              icon: UnlinkListIcon,
            },
          },
          textAlign: {
            inDropdown: true,
          },
          fontFamily: {
            options: ['Poppins', 'Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana'],
          },
          image: {
            icon: AttachmentIcon,
            uploadCallback: uploadImageCallBack,
            previewImage,
            component: imageComponent,
            inputAccept: 'image/jpeg,image/jpg,image/png,image/svg+xml,application/pdf',
            alt: { present: true, mandatory: false },
            defaultSize: {
              height: 'auto',
              width: '550',
            },
          },
          emoji: {
            className: undefined,
            component: undefined,
            popupClassName: undefined,
            emojis: [
              '🙂', '😀', '😁', '😂', '😃', '😉', '😋', '😎', '😍', '😗', '🤗', '🤔', '😣', '😫', '😴', '😌', '🤓',
              '😛', '😜', '😠', '😇', '😷', '😈', '👻', '😺', '😸', '😹', '😻', '😼', '😽', '🙀', '🙈',
              '🙉', '🙊', '👼', '👮', '🕵', '💂', '👳', '🎅', '👸', '👰', '👲', '🙍', '🙇', '🚶', '🏃', '💃',
              '⛷', '🏂', '🏌', '🏄', '🚣', '🏊', '⛹', '🏋', '🚴', '👫', '💪', '👈', '👉', '👉', '👆', '🖕',
              '👇', '🖖', '🤘', '🖐', '👌', '👍', '👎', '✊', '👊', '👏', '🙌', '🙏', '🐵', '🐶', '🐇', '🐥',
              '🐸', '🐌', '🐛', '🐜', '🐝', '🍉', '🍄', '🍔', '🍤', '🍨', '🍪', '🎂', '🍰', '🍾', '🍷', '🍸',
              '🍺', '🌍', '🚑', '⏰', '🌙', '🌝', '🌞', '⭐', '🌟', '🌠', '🌨', '🌩', '⛄', '🔥', '🎄', '🎈',
              '🎉', '🎊', '🎁', '🎗', '🏀', '🏈', '🎲', '🔇', '🔈', '📣', '🔔', '🎵', '🎷', '💰', '🖊', '📅',
              '✅', '❎', '💯',
            ],
          },
        }}
        toolbarCustomButtons={[
          <button
            type="button"
            className={`wysiwyg-formik__toggle ${removeExtraOptions ? 'wysiwyg-formik__toggle_remove-options' : ''}`}
            onClick={() => setShowExtraToolbar(!showExtraToolbar)}
          >
            <EditIcon />
          </button>,
          <SubmitEditor
            buttonCallback={buttonCallback}
            resetEditorState={() => resetEditorState(setEditorState)}
            disabled={isDisabled}
          />,
        ]}
        mention={{
          separator: ' ',
          trigger: '@',
          suggestions,
        }}
        blockRendererFn={(block) => customBlockRendererFn?.(block, editorState)}
      />
      {error && <span className="wysiwyg-formik__error">{error}</span>}
    </div>
  );
};

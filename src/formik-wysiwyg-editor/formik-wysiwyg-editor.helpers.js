import { EditorState, ContentState, getDefaultKeyBinding, convertFromHTML } from 'draft-js';

export const updateEditorValue = (value) => {
  const blocksFromHTML = convertFromHTML(value);
  const convertedState = ContentState.createFromBlockArray(
    blocksFromHTML.contentBlocks,
    blocksFromHTML.entityMap
  );
  return EditorState.createWithContent(convertedState);
}

export const initializeEditorValue = (value) => {
  if (!value) {
    return EditorState.createEmpty();
  }

  return updateEditorValue(value);
}

export const resetEditorState = (setEditorState) => {
  const resetEditor = EditorState.createEmpty();
  setEditorState(EditorState.moveFocusToEnd(resetEditor));
}

export const onEnterHandler = (e, handleSubmit, setEditorState, setIsReply, isDisabled) => {
  if (e.keyCode === 13) {
    if (isDisabled) {
      return;
    }
    handleSubmit();
    resetEditorState(setEditorState);
    setIsReply?.(false);
    return;
  }
  return getDefaultKeyBinding(e);
}

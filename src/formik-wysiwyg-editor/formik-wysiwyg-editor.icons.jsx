import React from 'react';

export const EditIcon = () => (
  <svg width="10" height="10" viewBox="0 0 426.667 426.667" fill="#9C9C9C">
    <g>
      <g>
        <circle cx="42.667" cy="213.333" r="42.667"/>
      </g>
    </g>
    <g>
      <g>
        <circle cx="213.333" cy="213.333" r="42.667"/>
      </g>
    </g>
    <g>
      <g>
        <circle cx="384" cy="213.333" r="42.667"/>
      </g>
    </g>
  </svg>
)

export const CloseIcon = () => (
  <svg width="20" height="20" viewBox="0 0 24 24" fill="#3B3C3B" >
    <path d="M19 6.41L17.59 5L12 10.59L6.41 5L5 6.41L10.59 12L5 17.59L6.41 19L12 13.41L17.59 19L19 17.59L13.41 12L19 6.41Z" />
  </svg>
);

import {
  ABOUT_PERCENT,
  FIRST_NAME_PERCENT,
  LAST_NAME_PERCENT,
  PROFILE_PICTURE_PERCENT,
  SKILL_SET_PERCENT,
  TAGLINE_PERCENT, USER_NAME_PERCENT, VERIFY_EMAIL_PERCENT,
} from "./profile-info.constants";

export const getFieldPercent = (field, percent) => {
  if (field) {
    return percent;
  }

  return 0;
};

export const calculateProfileCompletion = (profile) => {
  const {
    skills, avatarUrl, userName, emails,
  } = profile || {};
  const profileCompletionDict = {
    firstName: FIRST_NAME_PERCENT,
    lastName: LAST_NAME_PERCENT,
    aboutMe: ABOUT_PERCENT,
    tagline: TAGLINE_PERCENT,
  };

  let profileCompletion = 0;

  /*
    calculate completion from profile user object
  */
  Object.keys(profileCompletionDict).forEach((key) => {
    // @ts-ignore
    profileCompletion += getFieldPercent(profile[key], profileCompletionDict[key]);
  });

  /*
    calculate completion when user have 5 skills
  */
  profileCompletion += getFieldPercent(skills?.values?.length >= 5, SKILL_SET_PERCENT);

  /*
    calculate completion when user has avatar
  */
  profileCompletion += getFieldPercent(avatarUrl, PROFILE_PICTURE_PERCENT);

  /*
    calculate completion when user has username
  */
  profileCompletion += getFieldPercent(userName, USER_NAME_PERCENT);

  /*
    calculate completion when user has verified email
  */
  profileCompletion += getFieldPercent(emails?.some((email) => email.verified), VERIFY_EMAIL_PERCENT);

  // TODO: add calculate completion for first task, first project, first client, first comment

  return profileCompletion;
};

import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import { LoadingSmallDots } from "../../loaders";
import { calculateProfileCompletion } from "./profile-info.helpers";
import { defaultAvatarUrl } from "./profile-info.constants";
import {defaultTheme} from "../../palette";

export const ProfileInfo = ({ profile, isProfileLoading }) => {
  const { name, tagline, avatarUrl } = profile || {};

  const [completionPercent, setCompletionPercent] = useState(0);

  useEffect(() => {
    if (profile) {
      setCompletionPercent(calculateProfileCompletion(profile));
    }
  }, [profile]);

  if (isProfileLoading) {
    return (
      <Wrapper>
        <LoadingSmallDots />
      </Wrapper>
    );
  }

  return (
    <Wrapper>
      <Avatar src={avatarUrl || defaultAvatarUrl} />
      <Name>{name}</Name>
      <Tagline>{tagline}</Tagline>
      <Completion>
        <CompletionDescription>
          <CompletionTitle>Profile completion</CompletionTitle>
          <CompletionValue>{`${completionPercent}%`}</CompletionValue>
        </CompletionDescription>
        <CompletionProgressWrapper>
          <CompletionProgress progress={completionPercent} />
        </CompletionProgressWrapper>
      </Completion>
    </Wrapper>
  );
};

// region styles
const Wrapper = styled("div")`
  width: 335px;
  padding: 45px 55px;
  margin: 0 0 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  flex-shrink: 0;
  background-color: ${defaultTheme.background};
  box-shadow: 0 0 10px hsl(0deg 0% 60% / 45%);
  border-radius: 6px;
`;

const Avatar = styled("div")`
  margin-bottom: 15px;
  width: 184px;
  height: 184px;
  border-radius: 50%;
  border: 5px solid transparent;
  position: relative;
  background-repeat: no-repeat;
  background-position: 50%;
  background-size: cover;
  background-color: #e5e6e8;
  background-clip: padding-box;
  background-image: ${({ src }) => `url("${src}")`};
`;

const Name = styled("p")`
  margin-bottom: 10px;
  font-size: 25px;
  font-weight: 500;
`;

const Tagline = styled("p")`
  padding-bottom: 25px;
  margin-bottom: 30px;
  align-self: stretch;
  border-bottom: 1px solid ${defaultTheme.border};
  color: #ec9b37;
  font-size: 15px;
  font-weight: 300;
  text-align: center;
  overflow: hidden;
`;

const Completion = styled("div")`
  align-self: stretch;
`;

const CompletionDescription = styled("p")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 15px;
`;

const CompletionTitle = styled("span")`
  font-size: 14px;
  font-weight: 500;
`;

const CompletionValue = styled("span")`
  font-size: 15px;
  font-weight: 600;
`;

const CompletionProgressWrapper = styled("div")`
  height: 15px;
  overflow: hidden;
  box-shadow: inset 0 0.5px 3px rgb(0 0 0 / 20%), inset 0 0 4px rgb(0 0 0 / 10%);
  border-radius: 5px;
  border: 1px solid #c3c3c3;
`;

const CompletionProgress = styled("div")`
  height: 100%;
  width: ${({ progress }) => `${progress}%`};
  transition: width .6s linear;
  background-image: linear-gradient(270deg,${defaultTheme.accent} 10.52%,#ff3d00);
`;

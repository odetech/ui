import React from 'react';
import { ProfileInfo as Profile } from './profile-info.component';
import { user } from '../../mocks/user';

export default {
  component: Profile,
  title: 'Profile',
};

const Template = (args) => (
  <div style={{ width: '335px' }}>
    <Profile {...args} />
  </div>
);

export const ProfileInfo = Template.bind({});
ProfileInfo.args = {
  profile: {
    ...user.profile,
    avatarUrl: user.profile?.avatar?.secureUrl,
    name: `${user.profile.firstName} ${user.profile.lastName}`,
    skills: user.profile?.skillsets,
  },
  isProfileLoading: false,
};

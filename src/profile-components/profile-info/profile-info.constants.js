export const FIRST_NAME_PERCENT = 2;
export const LAST_NAME_PERCENT = 2;
export const USER_NAME_PERCENT = 4;
export const VERIFY_EMAIL_PERCENT = 9;
export const TAGLINE_PERCENT = 3;
export const ABOUT_PERCENT = 8;
export const SKILL_SET_PERCENT = 18;
export const PROFILE_PICTURE_PERCENT = 11;

// TODO: remove it
export const defaultAvatarUrl = "https://sbcf.fr/wp-content/uploads/2018/03/sbcf-default-avatar.png";

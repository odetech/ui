import React, { useState } from "react";
import { Field } from "formik";
import styled from "@emotion/styled";
import { ClosedEyeIcon, OpenedEyeIcon } from "../profile-overview/icons.jsx";
import {defaultTheme} from "../../palette";

export const BaseInput = ({
  inputProps,
  marginBottom,
  component: Component,
  label,
  labelStyle,
  width,
  margin,
  withPasswordIcon,
}) => {
  const [isVisible, setVisible] = useState(false);

  return (
    <Field name={inputProps.name}>
      {({ field, meta }) => {
        const { error } = meta;
        const { type } = inputProps;

        return (
          <Label marginBottom={marginBottom} width={width} margin={margin}>
            {label && <StyledLabelText style={labelStyle}>{label}</StyledLabelText>}
            <StyledInput
              {...inputProps}
              type={(isVisible && inputProps.type === "password") ? "text" : (inputProps.type || "text")}
              {...field}
              error={error}
            />
            <ErrorStyled>{error}</ErrorStyled>
            {type === "password" && withPasswordIcon && (
              <IconButton type="button" onClick={() => setVisible(!isVisible)} tabIndex={-1}>
                {isVisible ? <OpenedEyeIcon /> : <ClosedEyeIcon />}
              </IconButton>
            )}
          </Label>
        );
      }}
    </Field>
  );
};

// region styles
const Label = styled("label")`
  position: relative;

  width: ${({ width }) => width ?? "100%"};
  display: inline-flex;
  flex-direction: column;
  align-items: flex-start;
  padding-bottom: 19px;
  ${({ margin }) => margin && `margin: ${margin}`};
  ${({ marginBottom }) => marginBottom && `margin-bottom: ${marginBottom}px;`};
`;

const ErrorStyled = styled("span")`
  position: absolute;
  left: 0;
  bottom: 3px;

  color: red;
  font-size: 13px;
`;

const StyledLabelText = styled("span")`
  margin-bottom: 10px;

  font-size: 14px;
`;

const IconButton = styled("button")`
  background-color: transparent;
  border: none;
  position: absolute;
  right: 0;
  bottom: 20px;
  transition: opacity .3s;

  &:hover {
    opacity: 0.8;
  }
`;

const StyledInput = styled("input")`
  width: 100%;
  padding: 5px;

  border: none;
  border-bottom: 1px solid ${defaultTheme.label};

  line-height: 1.5;
  font-size: 14px;
  font-weight: 500;
  color: #3b3b3b;

  transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;

  ${({ error }) => error && "border-color: red;"};

  &:hover:not(:disabled):not(:read-only) {
    border-color: rgba(247, 148, 42, 0.5);
  }

  &:focus:not(:read-only) {
    background-color: rgba(247, 148, 42, 0.1);
    border-color: rgba(247, 148, 42, 0.5);
  }

  &:disabled {
    opacity: 0.5;

    background-color: #f7f7f7;

  }

  &:disabled,
  &:read-only {
    cursor: not-allowed;
  }
`;

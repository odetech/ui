import { ClipLoader } from 'react-spinners';
import React from 'react';
import styled from '@emotion/styled';
import { ConfirmIcon, CancelIcon } from '../profile-overview/icons.jsx';
import {defaultTheme} from "../../palette";

export const FormButton = ({
  styleType, margin, isLoading, label, ...rest
}) => (
  <StyledButton
    margin={margin}
    actionColors={actionColors[styleType]}
    {...rest}
  >
    {styleType === "confirm" ? <ConfirmIcon/> : <CancelIcon />}
    {label}
    {isLoading && <ClipLoader color="white" size={16} />}
  </StyledButton>
);

const actionColors = {
  cancel: {
    base: "#F33822",
    hover: "#F2796A",
    active: "#C92B18",
  },
  confirm: {
    base: defaultTheme.green,
    hover: defaultTheme.greenHover,
    active: defaultTheme.greenFocus,
  },
};

const getBoxShadow = (color) => `box-shadow: 0 0 2px ${color}`;

// region styles
const StyledButton = styled("button")`
  position: relative;

  display: inline-flex;
  align-items: center;
  padding: 12px 30px;
  white-space: nowrap;
  ${({ margin }) => margin && `margin: ${margin}`};

  background-color: ${({ actionColors }) => actionColors.base};
  ${({ actionColors }) => getBoxShadow(actionColors.base)};
  border-radius: 7px;

  color: ${defaultTheme.background};
  font-size: 15px;

  transition: background-color .500ms;

  &:hover {
    background-color: ${({ actionColors }) => actionColors.hover};
    ${({ actionColors }) => getBoxShadow(actionColors.hover)};
  }

  &:active {
    background-color: ${({ actionColors }) => actionColors.active};
    ${({ actionColors }) => getBoxShadow(actionColors.active)};
  }

  &:disabled {
    background-color: ${defaultTheme.disabled};
    ${getBoxShadow(defaultTheme.disabled)};

    color: ${defaultTheme.label};

    cursor: not-allowed;
  }

  & svg {
    margin-right: 5px;
    fill: white;

    &:hover {
      fill: white;
    }
  }

  & > span {
    position: absolute;
    right: 10px;
    top: calc(50% - 8px);
  }
`;

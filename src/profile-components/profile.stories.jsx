import React, { useState } from 'react';
import { user } from '../mocks/user';
import { Overview } from './profile-overview';
import { ProfileInfo } from './profile-info';
import { InvitationModal, Networks } from '../networks-components';
import WrapperWithDefaultStyles from '../stories/default-stories-components/WrapperWithDefaultStyles';

const ProfilePage = ({
  profile, isMe, isProfileLoading, isFormSubmitting, taglineSubmit, aboutSubmit, skillsSubmit, settingsSubmit,
  networksData, isNetworksLoading,
}) => {
  const [showNetworkEmailInviteModal, setShowNetworkEmailInviteModal] = useState(false);

  return (
    <WrapperWithDefaultStyles>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: 20 }}>
          <div>
            <ProfileInfo
              profile={profile}
              isProfileLoading={isProfileLoading}
            />
          </div>
          <div>
            <InvitationModal
              validation={null}
              defaultInviteEmail="inputEmail@odecloud.com"
              userFirstName="Someone"
              showModal={showNetworkEmailInviteModal}
              setShowModal={setShowNetworkEmailInviteModal}
              onSubmit={(payloadData) => {
                console.log('payloadData', {
                  ...payloadData,
                  emails: payloadData.emails.filter((email) => email !== ''),
                });
              }}
            />
          </div>
          <div>
            <Networks
              validation={null}
              networksAmount={networksData.networksCount.value}
              networksData={networksData.networks}
              loading={isNetworksLoading}
              fetchNetworks={(currentPage) => {
                console.log('currentPage', currentPage);
              }}
              onSubmit={(email) => {
                console.log('email', email);
                setShowNetworkEmailInviteModal(true);
              }}
              onUserClick={(userData) => {
                console.log('On User click', userData);
              }}
              cardPadding="20px 45px"
              userButtonMargin="0 34px 10px 0"
              usersWrapperMargin="0 -34px 0 0"
            />
          </div>
        </div>
        <div style={{ width: '100%' }}>
          <Overview
            profile={profile}
            isMe={isMe}
            isFormSubmitting={isFormSubmitting}
            isProfileLoading={isProfileLoading}
            taglineSubmit={taglineSubmit}
            aboutSubmit={aboutSubmit}
            skillsSubmit={skillsSubmit}
            settingsSubmit={settingsSubmit}
          />
        </div>
      </div>
    </WrapperWithDefaultStyles>
  );
};

export default {
  component: ProfilePage,
  title: 'Profile',
};

const Template = (args) => <ProfilePage {...args} />;

export const Profile = Template.bind({});
Profile.args = {
  profile: {
    ...user.profile,
    avatarUrl: user.profile?.avatar?.secureUrl,
    skills: user.profile?.skillsets,
    emails: user.emails,
    userName: user.username,
    name: `${user.profile.firstName} ${user.profile.lastName}`,
  },
  isMe: true,
  taglineSubmit: (data) => { console.log(data); },
  aboutSubmit: (data) => { console.log(data); },
  skillsSubmit: (data) => { console.log(data); },
  settingsSubmit: (data) => { console.log(data); },
  isFormSubmitting: false,
  isProfileLoading: false,
  isNetworksLoading: false,
  networksData: {
    networks: [{
      _id: 'FtqkruHdPJ48PrSvz',
      isPending: false,
      userId1: 'dGB3KgwYDv6mCsNrT',
      userId2: '106e7a63-e32c-4304-b698-5a0c2ee1e36e',
      createdAt: '2021-10-25T00:42:01.351000',
      updatedAt: '2021-09-07T21:44:52.351000',
      user2: {
        _id: '106e7a63-e32c-4304-b698-5a0c2ee1e36e',
        createdAt: '2021-09-21T06:05:24.906023',
        profile: {
          firstName: 'Danny', lastName: 'Nguyen', createdBy: null, tagline: null, aboutMe: null, createdFrom: null, avatar: { _id: 'u2AngcxzGby65pzAM', secureUrl: 'https://res.cloudinary.com/https-platform-odecloud-com/image/upload/v1634865821/production/avatars/qpu832z8ywjpnymeeo4z.jpg' }, skillsets: null, roles: null, masterNotifications: null
        },
        services: { password: { bcrypt: null }, resume: { loginTokens: [], websocketTokens: [], passwordlessTokens: [] }, email: { verificationTokens: [] } },
        username: 'danny-nguyen-5714-9825-5264',
        emails: [{ address: 'danny@odecloud.com', verified: true }]
      }
    }, {
      _id: 'oXxLf32ZfZ4vMRMXm',
      isPending: false,
      userId1: 'dGB3KgwYDv6mCsNrT',
      userId2: 'FyvaRdDCFgwf7kTRw',
      createdAt: '2021-06-11T02:55:06.846000',
      updatedAt: '2021-06-11T02:55:06.846000',
      user2: {
        _id: 'FyvaRdDCFgwf7kTRw',
        createdAt: '2019-02-18T23:30:18.396000',
        profile: {
          firstName: 'Osar', lastName: 'Iyamu', createdBy: null, tagline: 'Co-founder, CEO @OdeCloud', aboutMe: 'I help build high-performing NetSuite teams at OdeCloud. 12 years Enterprise Resource Planning (ERP) consulting experience, including 5 years with Deloitte Consulting, the world’s largest, top-rated technology consulting firm. and lead roles (Project Management, Planning and Scoping, Design, Configuration, Development Coordination, Training and Post Go-live support), demonstrating particular expertise in NetSuite: All ERP standard and advanced modules, CRM and Workflows using SuiteFlows, as well as SAP: Sales and Distribution with integration to Material Management, Warehouse Management System, Customer Service, Finance and Controlling.   His consulting experiences have enabled him to acquire a sophisticated appreciation of Financial and Logistic business processes to assist business in strategic projects in the areas of Finance & Accounting, CRM, Order-To-Cash, Procure-To-Pay, Inventory, Demand and Supply Chain Management.  ', createdFrom: null, avatar: { _id: 'XMvTs8myzGXfagD62', secureUrl: 'https://res.cloudinary.com/https-platform-odecloud-com/image/upload/v1616650651/stage/avatars/jh7h38ehqvmatrpomqrm.png' }, skillsets: null, roles: null, masterNotifications: null
        },
        services: { password: { bcrypt: null }, resume: { loginTokens: [], websocketTokens: [], passwordlessTokens: [] }, email: { verificationTokens: [] } },
        username: 'Osar',
        emails: [{ address: 'osar@odecloud.com', verified: true }]
      }
    }, {
      _id: 'rQjiKujad6aAEWPSm-3240978slkdfjlksjf',
      isPending: false,
      userId1: 'dGB3KgwYDv6mCsNrT',
      userId2: 'SG8aYLxtAK7cbpakm',
      createdAt: '2021-04-12T14:10:22.985000',
      updatedAt: '2021-04-12T14:10:22.985000',
      user2: {
        _id: 'SG8aYLxtAK7cbpakm',
        createdAt: '2018-12-01T22:35:54.257000',
        profile: {
          firstName: 'Vanielle', lastName: 'Lee', createdBy: null, tagline: 'CTO @ OdeCloud', aboutMe: 'What would you like to know about me?', createdFrom: null, avatar: { _id: 'a8dETn3XwTtvgmdb3', secureUrl: 'https://res.cloudinary.com/https-platform-odecloud-com/image/upload/v1624668634/production/avatars/ul72lt4diw9amcgxyhno.jpg' }, skillsets: null, roles: null, masterNotifications: null
        },
        services: { password: { bcrypt: null }, resume: { loginTokens: [], websocketTokens: [], passwordlessTokens: [] }, email: { verificationTokens: [] } },
        username: 'leevanielle',
        emails: [{ address: 'vanielle@odecloud.com', verified: true }]
      }
    }, {
      _id: '7Yf2PKS3m22B38ddY',
      isPending: false,
      userId1: 'dGB3KgwYDv6mCsNrT',
      userId2: 'Wj2PKhcSacD3YiHaP',
      createdAt: '2021-02-24T08:10:03.535000',
      updatedAt: '2021-02-24T08:10:03.535000',
      user2: {
        _id: 'Wj2PKhcSacD3YiHaP',
        createdAt: '2020-11-17T03:15:10.823000',
        profile: {
          firstName: 'Ivan', lastName: 'Ivanov', createdBy: null, tagline: 'I am a developer', aboutMe: "Time when i'm available: 04:00am - 02:30pm", createdFrom: null, avatar: { _id: 'X9oivpixupMeeG5Mi', secureUrl: 'https://res.cloudinary.com/https-platform-odecloud-com/image/upload/v1617796628/stage/avatars/kbdvvksokznapyygo4ou.jpg' }, skillsets: null, roles: null, masterNotifications: null
        },
        services: { password: { bcrypt: null }, resume: { loginTokens: [], websocketTokens: [], passwordlessTokens: [] }, email: { verificationTokens: [] } },
        username: 'ivan-91299-54155',
        emails: [{ address: 'ivan.kb@bk.ru', verified: true }]
      }
    }, {
      _id: '3cc1c943-53cc-4d72-bcfa-76e580f01d4f',
      isPending: false,
      userId1: 'dGB3KgwYDv6mCsNrT',
      userId2: 'XSMJv5beWFRAgfqg2',
      createdAt: null,
      updatedAt: '2021-08-19T13:22:21.290887',
      user2: {
        _id: 'XSMJv5beWFRAgfqg2',
        createdAt: '2020-04-29T05:14:38.192000',
        profile: {
          firstName: 'Team', lastName: 'OdeCloud', createdBy: null, tagline: 'Solution Architect', aboutMe: "I've been working with NetSuite since 2010 - first as a sales rep user, then a sales ops policy and business control designer, then a full NS admin for a company with 300 users before making the jump to consultant (5 years) and then finally going solo (2 years)", createdFrom: null, avatar: { _id: 'cXt8hLE5Se9cFy9Lr', secureUrl: 'https://res.cloudinary.com/https-platform-odecloud-com/image/upload/v1589557576/production/avatars/uby1aw0yxbsurw4qqmvq.jpg' }, skillsets: null, roles: null, masterNotifications: null
        },
        services: { password: { bcrypt: null }, resume: { loginTokens: [], websocketTokens: [], passwordlessTokens: [] }, email: { verificationTokens: [] } },
        username: 'nassim-67574-43876',
        emails: [{ address: 'team@odecloud.com', verified: true }]
      }
    }, {
      _id: '5f68f858-729d-497e-b416-c0311e71ffbe',
      isPending: false,
      userId1: 'dGB3KgwYDv6mCsNrT',
      userId2: 'eTAspwGsbcWdC6ciF',
      createdAt: null,
      updatedAt: '2021-09-15T06:47:31.635166',
      user2: {
        _id: 'eTAspwGsbcWdC6ciF',
        createdAt: '2021-09-15T06:42:34.663000',
        profile: {
          firstName: 'Zhanna', lastName: 'Firchuk', createdBy: null, tagline: 'Developer?', aboutMe: '🐈🐈🐈', createdFrom: null, avatar: { _id: 'pWKQ8wCALqnMGehtp', secureUrl: 'https://res.cloudinary.com/https-platform-odecloud-com/image/upload/v1631688203/production/avatars/fc1gei6pgkishdinjl3z.png' }, skillsets: null, roles: null, masterNotifications: null
        },
        services: { password: { bcrypt: null }, resume: { loginTokens: [], websocketTokens: [], passwordlessTokens: [] }, email: { verificationTokens: [] } },
        username: 'zhanna-86352-25274',
        emails: [{ address: 'zhfirchuk@aspirity.com', verified: true }]
      }
    }, {
      _id: '25ee4e87-cd49-442c-a6c2-f0b4b9cec726',
      isPending: false,
      userId1: 'dGB3KgwYDv6mCsNrT',
      userId2: 'w4uk84PxC4htsotvq',
      createdAt: null,
      updatedAt: '2021-09-14T07:56:28.754482',
      user2: {
        _id: 'w4uk84PxC4htsotvq',
        createdAt: '2020-10-30T06:49:18.993000',
        profile: {
          firstName: 'Tim', lastName: 'Akmatov', createdBy: null, tagline: '123', aboutMe: 'UX/UI', createdFrom: null, avatar: { _id: 'kbw9HhJwxMcu8T5AB', secureUrl: 'https://res.cloudinary.com/https-platform-odecloud-com/image/upload/v1604581907/production/avatars/ao9zb1gfwxuvstlpizo9.gif' }, skillsets: null, roles: null, masterNotifications: null
        },
        services: { password: { bcrypt: null }, resume: { loginTokens: [], websocketTokens: [], passwordlessTokens: [] }, email: { verificationTokens: [] } },
        username: 'Tikokoko',
        emails: [{ address: 'tmoorqa@gmail.com', verified: true }]
      }
    }, {
      _id: '17ba5904-c255-4f95-8f5d-ebaadc14a4f4',
      isPending: false,
      userId1: 'dGB3KgwYDv6mCsNrT',
      userId2: 'yuxftabtgoE4jmidK',
      createdAt: null,
      updatedAt: '2021-08-24T11:53:36.505139',
      user2: {
        _id: 'yuxftabtgoE4jmidK',
        createdAt: '2021-03-17T07:41:04.826000',
        profile: {
          firstName: 'Ravil', lastName: 'TestAccount', createdBy: null, tagline: null, aboutMe: null, createdFrom: null, avatar: { _id: 'w53ftP3hjtoqt6AW6', secureUrl: 'https://res.cloudinary.com/https-platform-odecloud-com/image/upload/v1615967361/stage/avatars/f2sqmfdvin8po8setvgr.png' }, skillsets: null, roles: null, masterNotifications: null
        },
        services: { password: { bcrypt: null }, resume: { loginTokens: [], websocketTokens: [], passwordlessTokens: [] }, email: { verificationTokens: [] } },
        username: 'ravil-58307-34756',
        emails: [{ address: 'rnazyrov.aspirity@gmail.com', verified: false }]
      }
    }],
    networksCount: { _id: 'dGB3KgwYDv6mCsNrT', value: 8 },
  },
};

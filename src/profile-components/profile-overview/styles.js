import styled from '@emotion/styled';
import { Form } from 'formik';
import { css } from '@emotion/react';
import { defaultTheme } from '../../palette';

export const CardStyles = css`
  padding: 25px 30px;
  border-radius: 6px;
  box-shadow: 0 0 34px hsl(0deg 0% 60% / 25%);
  background: ${defaultTheme.background};
`;

export const Card = styled('div')`
  ${CardStyles};
  ${({ margin }) => margin && `margin: ${margin}`};
`;

export const CardWrapper = styled('div')`
  ${CardStyles};

  padding: 20px 25px 20px 35px;
  display: flex;

  &:not(:last-of-type) {
    margin-bottom: 20px;
  }
`;

export const IconWrapper = styled('div')`
  padding-right: 65px;
  margin-right: 35px;
  border-right: 1px solid #e6e6e6;
  position: relative;

  & svg {
    position: absolute;
    top: ${({ svgTopPos }) => svgTopPos || '15px'};
    left: 0;
  }
`;

export const Title = styled('p')`
  font-size: 20px;
  font-weight: 600;
  line-height: 30px;
`;

export const CardTitle = styled('p')`
  font-size: 15px;
  font-weight: 600;
  text-transform: uppercase;
  display:flex;
  align-items:center;
  justify-content: space-between;
`;

export const EditButton = styled('button')`
  border: none;
  background-color: transparent;
  cursor:pointer;

  opacity: 0.7;
`;

export const Content = styled('p')``;

export const TitleWrapper = styled('div')`
  margin-bottom: 15px;
  font-weight: 600;
  font-size: 20px;
  border-bottom: 1px solid #e6e6e6;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const ContentWrapper = styled('div')`
  width: 100%;
`;

export const Skill = styled('div')`
  display:flex;
  align-items:center;
  padding: 6px 15px;
  background-color: ${defaultTheme.disabled};
  border-radius: 6px;
  margin-bottom: 5px;

  &:not(:last-of-type) {
    margin-right: 10px;
  }

  & button {
    margin-left: 5px;
    transition: opacity .3s;

    &:hover {
      opacity: 0.8;
    }
  }
`;

export const SkillsContent = styled('div')`
  display:flex;
  align-items:center;
  flex-wrap: wrap;
`;

export const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
  position: relative;
`;

export const AddSkillButton = styled('button')`
  padding: 2px 15px;
  background-color: rgba(247,148,42,.3);
  border: 1px solid ${defaultTheme.accent};
  border-radius: 7px;
  font-size: 13px;
  color: ${defaultTheme.accent};
  position: absolute;
  display:flex;
  align-items:center;
  top: 5px;
  right: 0;
  transition: opacity .3s;

  &:hover {
    opacity: 0.8;
  }

  & svg {
    margin-right: 5px;
  }
`;

export const FieldsWrapper = styled('div')`
  display:flex;
  justify-content: space-between;
  flex-wrap:wrap;
`;

export const SettingsTitle = styled('div')`
  font-size: 20px;
  font-weight: 600;
  line-height: 30px;
  margin-bottom: 20px;
`;

export const ButtonsWrapper = styled('div')`
  display:flex;
  align-items:center;
  padding: 15px 25px 15px 35px;
`;

export const SkillsSettingsWrapper = styled('div')`
  position: relative;
  width: 100%;
`;

export const PasswordCard = styled('div')`
  ${CardStyles};

  display:flex;
  align-items:center;
  justify-content: space-between;
`;

export const OverviewEditButton = styled('button')`
  padding: 5px 15px;
  background-color: rgba(247,148,42,.3);
  border: 1px solid ${defaultTheme.accent};
  border-radius: 7px;
  display:flex;
  align-items:center;
  font-size: 13px;
  color: ${defaultTheme.accent};
  transition: opacity .3s;

  &:hover:not(:disabled) {
    opacity: 0.8;
  }

  & svg {
    margin-right: 5px;
  }
`;

import React from 'react';
import {
  CloseIcon, EditIcon, PlusIcon, SkillsIcon,
} from './icons.jsx';
import { Formik } from 'formik';
import { BaseInput } from '../common/base-input.component.jsx';
import { FormButton } from '../common/button.component.jsx';
import {
  CardWrapper,
  IconWrapper,
  ContentWrapper,
  TitleWrapper,
  Title,
  EditButton,
  StyledForm,
  SkillsContent,
  Skill,
  AddSkillButton,
} from './styles';
import { defaultTheme } from '../../palette';

export const Skills = ({
  skills, isMe, isEdit, setActiveEdit, isLoading, submitCallback,
}) => {
  const initialValues = {
    skills: skills?.values,
    skill: "",
  };

  const addSkill = (
    skill, skillsValues, setFieldValue,
  ) => {
    if (!skill.trim()) {
      return;
    }

    if (skillsValues?.some((s) => s === skill)) {
      return;
    }
    const copySkills = [...skillsValues];
    copySkills.push(skill);
    setFieldValue("skills", copySkills);
    setFieldValue("skill", "");
  };

  const removeSkill = (
    skill, skillsValues, setFieldValue,
  ) => {
    const skillIndex = skillsValues?.findIndex((s) => s === skill);
    const copySkills = [...skillsValues];
    copySkills.splice(skillIndex, 1);

    setFieldValue("skills", copySkills);
  };

  return (
    <CardWrapper>
      <IconWrapper svgTopPos="20px">
        <SkillsIcon fill="#DD9831" />
      </IconWrapper>
      <ContentWrapper>
        <TitleWrapper>
          <Title>Skills</Title>
          {isMe && (
            <EditButton onClick={() => setActiveEdit(isEdit ? null : "skills")}>
              {isEdit
                ? <CloseIcon fill={defaultTheme.accent} />
                : <EditIcon fill={defaultTheme.accent} />}
            </EditButton>
          )}
        </TitleWrapper>
        {isEdit
          ? (
            <Formik
              initialValues={initialValues}
              onSubmit={async (values) => {
                await submitCallback(values, skills?.id);
                setActiveEdit(null);
              }}
            >
              {({ values, setFieldValue, handleSubmit }) => {
                const { skills: skillsValues, skill } = values;

                return (
                  <StyledForm onKeyDown={(e) => {
                    if (e.keyCode === 13) {
                      e.preventDefault();
                      addSkill(skill, skillsValues, setFieldValue);
                    }
                  }}
                  >
                    <BaseInput
                      inputProps={{ name: "skill", disabled: isLoading }}
                      width="100%"
                    />
                    <AddSkillButton
                      onClick={() => addSkill(skill, skillsValues, setFieldValue)}
                      type="button"
                    >
                      <PlusIcon fill={defaultTheme.accent} />
                      Add
                    </AddSkillButton>
                    <SkillsContent>
                      {skillsValues?.map((s) => (
                        <Skill key={s}>
                          {s}
                          <button type="button" onClick={() => removeSkill(s, skillsValues, setFieldValue)}>
                            <CloseIcon width="10px" height="10px" fill="rgb(59, 60, 59)" />
                          </button>
                        </Skill>
                      ))}
                    </SkillsContent>
                    <FormButton
                      isLoading={isLoading}
                      styleType="confirm"
                      onClick={handleSubmit}
                      type="button"
                      label="Save and Close"
                      margin="20px 0 0 auto"
                    />
                  </StyledForm>
                );
              }}
            </Formik>
          )
          : (
            <SkillsContent>
              {(skills?.values && skills?.values.length > 0) ? skills?.values?.map((skill) => (
                <Skill key={skill}>{skill}</Skill>
              )) : <p>No skills</p>}
            </SkillsContent>
          )}
      </ContentWrapper>
    </CardWrapper>
  );
};

import React from 'react';
import { Overview } from './overview.component';
import { user } from '../../mocks/user';

export default {
  component: Overview,
  title: 'Profile',
}

const Template = (args) => <Overview {...args} />;

export const ProfileOverview = Template.bind({});
ProfileOverview.args = {
  profile: {
    ...user.profile,
    skills: user.profile?.skillsets,
    emails: user.emails,
    userName: user.username,
  },
  isMe: true,
  // isMe: userData.info && pathnameParams.profileId === userData.info._id,
  taglineSubmit: (data) => {console.log(data)},
  aboutSubmit: (data) => {console.log(data)},
  skillsSubmit: (data) => {console.log(data)},
  settingsSubmit: (data) => {console.log(data)},
  isFormSubmitting: false,
  isProfileLoading: false,
};



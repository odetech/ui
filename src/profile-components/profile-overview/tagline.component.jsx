import React from 'react';
import { Formik } from 'formik';
import { BaseInput } from '../common/base-input.component.jsx';
import { CloseIcon, EditIcon, TaglineIcon } from './icons.jsx';
import {
  CardWrapper, IconWrapper, ContentWrapper, TitleWrapper, Title, EditButton, Content, StyledForm,
} from './styles';
import { FormButton } from '../common/button.component.jsx';
import {defaultTheme} from '../../palette';

export const Tagline = ({
  tagline, isMe, isEdit, setActiveEdit, isLoading, submitCallback,
}) => {
  const initialValues = {
    tagline,
  };

  return (
    <CardWrapper>
      <IconWrapper svgTopPos="7px">
        <TaglineIcon fill="#DD9831" />
      </IconWrapper>
      <ContentWrapper>
        <TitleWrapper>
          <Title>Tagline</Title>
          {isMe && (
            <EditButton onClick={() => setActiveEdit(isEdit ? null : "tagline")}>
              {isEdit
                ? <CloseIcon fill={defaultTheme.accent} />
                : <EditIcon fill={defaultTheme.accent} />}
            </EditButton>
          )}
        </TitleWrapper>
        {isEdit
          ? (
            <Formik
              initialValues={initialValues}
              onSubmit={async (values) => {
                await submitCallback(values);
                setActiveEdit(null);
              }}
            >
              {() => (
                <StyledForm>
                  <BaseInput
                    inputProps={{ name: "tagline", disabled: isLoading }}
                    width="100%"
                  />
                  <FormButton
                    isLoading={isLoading}
                    styleType="confirm"
                    label="Save and Close"
                    margin="20px 0 0 auto"
                  />
                </StyledForm>
              )}
            </Formik>
          )
          : <Content>{tagline || "Please fill Tagline section"}</Content>}
      </ContentWrapper>
    </CardWrapper>
  );
};

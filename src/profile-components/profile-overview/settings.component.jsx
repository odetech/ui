import { Form, Formik } from "formik";
import React from "react";
import { BaseInput } from "../common/base-input.component.jsx";
import { CloseIcon, PlusIcon } from "./icons.jsx";
import {
  FieldsWrapper,
  SettingsTitle,
  ButtonsWrapper,
  AddSkillButton,
  SkillsContent,
  Skill,
  SkillsSettingsWrapper,
  PasswordCard,
  Card,
} from "./styles";
import { FormButton } from '../common/button.component.jsx';
import {defaultTheme} from "../../palette";

export const Settings = ({ profile, setActiveEdit, onSubmitCallback, isLoading }) => {
  const initialValues = {
    firstName: profile?.firstName,
    lastName: profile?.lastName,
    userName: profile?.userName,
    email: profile?.emails[0]?.address,
    tagline: profile?.tagline,
    aboutMe: profile?.aboutMe,
    skills: profile?.skills?.values,
    skill: "",
    oldPassword: "",
    newPassword: "",
    confirmPassword: "",
  };

  const addSkill = (
    skill, skillsValues, setFieldValue,
  ) => {
    if (!skill.trim()) {
      return;
    }

    if (skillsValues?.some((s) => s === skill)) {
      return;
    }
    const copySkills = [...skillsValues];
    copySkills.push(skill);
    setFieldValue("skills", copySkills);
    setFieldValue("skill", "");
  };

  const removeSkill = (
    skill, skillsValues, setFieldValue,
  ) => {
    const skillIndex = skillsValues?.findIndex((s) => s === skill);
    const copySkills = [...skillsValues];
    copySkills.splice(skillIndex, 1);

    setFieldValue("skills", copySkills);
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={async (values) => {
        await onSubmitCallback(values);
        setActiveEdit(null);
      }}
    >
      {({ values, setFieldValue }) => {
        const { skills: skillsValues, skill } = values;

        return (
          <Form
            onKeyDown={(e) => {
              if (e.keyCode === 13) {
                e.preventDefault();
                addSkill(skill, skillsValues, setFieldValue);
              }
            }}
          >
            <Card margin="0 0 20px">
              <SettingsTitle>Personal Information</SettingsTitle>
              <FieldsWrapper>
                <BaseInput label="First Name" inputProps={{ name: "firstName" }} width="45%" />
                <BaseInput label="Last Name" inputProps={{ name: "lastName" }} width="45%" />
                <BaseInput label="Username" inputProps={{ name: "userName", disabled: true }} width="45%" />
                <BaseInput label="Email" inputProps={{ name: "email", disabled: true }} width="45%" />
                <BaseInput label="Tagline" inputProps={{ name: "tagline" }} />
                <BaseInput label="About" inputProps={{ name: "aboutMe" }} />
                <SkillsSettingsWrapper>
                  <BaseInput
                    inputProps={{ name: "skill" }}
                    width="100%"
                  />
                  <AddSkillButton
                    onClick={() => addSkill(skill, skillsValues, setFieldValue)}
                    type="button"
                  >
                    <PlusIcon fill={defaultTheme.accent} />
                    Add
                  </AddSkillButton>
                  <SkillsContent>
                    {skillsValues?.map((s) => (
                      <Skill key={s}>
                        {s}
                        <button type="button" onClick={() => removeSkill(s, skillsValues, setFieldValue)}>
                          <CloseIcon width="10px" height="10px" fill="rgb(59, 60, 59)" />
                        </button>
                      </Skill>
                    ))}
                  </SkillsContent>
                </SkillsSettingsWrapper>
              </FieldsWrapper>
            </Card>
            <PasswordCard>
              <BaseInput label="Old Password" inputProps={{ name: "oldPassword", type: "password" }} width="30%" withPasswordIcon />
              <BaseInput label="New Password" inputProps={{ name: "newPassword", type: "password" }} width="30%" withPasswordIcon />
              <BaseInput label="Confirm New Password" inputProps={{ name: "confirmPassword", type: "password" }} width="30%" withPasswordIcon />
            </PasswordCard>
            <ButtonsWrapper>
              <FormButton
                styleType="cancel"
                type="button"
                label="Close"
                onClick={() => setActiveEdit(null)}
              />
              <FormButton
                isLoading={isLoading}
                styleType="confirm"
                label="Save and Close"
                margin="0 0 0 auto"
              />
            </ButtonsWrapper>
          </Form>
        );
      }}
    </Formik>
  );
};

import React, { Fragment, useState } from "react";
import { LoadingSmallDots } from "../../loaders";
import { SettingsIcon } from "./icons.jsx";
import { About } from "./about.component.jsx";
import { Skills } from "./skills.component.jsx";
import { Tagline } from "./tagline.component.jsx";
import { Settings } from "./settings.component.jsx";
import { CardTitle, OverviewEditButton, Card } from "./styles";
import { defaultTheme } from "../../palette";

export const Overview = ({
  profile, isMe, isProfileLoading, isFormSubmitting, taglineSubmit, aboutSubmit, skillsSubmit, settingsSubmit,
}) => {
  const [activeEdit, setActiveEdit] = useState(null);
  const { tagline, aboutMe, skills } = profile || {};

  const renderContent = () => {
    if (isProfileLoading) {
      return (
        <LoadingSmallDots />
      );
    }

    if (activeEdit === "settings") {
      return (
        <Settings
          profile={profile}
          setActiveEdit={setActiveEdit}
          isLoading={isFormSubmitting}
          onSubmitCallback={settingsSubmit}
        />
      );
    }

    return (
      <Fragment>
        <Tagline
          tagline={tagline}
          isMe={isMe}
          setActiveEdit={setActiveEdit}
          isEdit={activeEdit === "tagline"}
          isLoading={isFormSubmitting}
          submitCallback={taglineSubmit}
        />
        <About
          aboutMe={aboutMe}
          isMe={isMe}
          setActiveEdit={setActiveEdit}
          isEdit={activeEdit === "about"}
          isLoading={isFormSubmitting}
          submitCallback={aboutSubmit}
        />
        <Skills
          skills={skills}
          isMe={isMe}
          setActiveEdit={setActiveEdit}
          isEdit={activeEdit === "skills"}
          isLoading={isFormSubmitting}
          submitCallback={skillsSubmit}
        />
      </Fragment>
    );
  };

  return (
    <div>
      <Card margin="0 0 20px 0">
        <CardTitle>
          Overview
          {isMe && (
            <OverviewEditButton
              onClick={() => setActiveEdit("settings")}
              disabled={activeEdit === "settings"}
            >
              <SettingsIcon fill={defaultTheme.accent} />
              {activeEdit === "settings" ? "Editing..." : "Edit settings"}
            </OverviewEditButton>
          )}
        </CardTitle>
      </Card>
      {renderContent()}
    </div>
  );
};

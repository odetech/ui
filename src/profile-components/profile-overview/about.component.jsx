import React from 'react';
import { CloseIcon, EditIcon, AboutIcon } from './icons.jsx';
import { Formik } from 'formik';
import { BaseInput } from '../common/base-input.component.jsx';
import {
  CardWrapper,
  IconWrapper,
  ContentWrapper,
  TitleWrapper,
  Title,
  EditButton,
  Content,
  StyledForm,
} from './styles';
import { FormButton } from '../common/button.component.jsx';
import { defaultTheme } from '../../palette';

export const About = ({
  aboutMe, isMe, isEdit, setActiveEdit, isLoading, submitCallback,
}) => {
  const initialValues = {
    aboutMe,
  };

  return (
    <CardWrapper>
      <IconWrapper>
        <AboutIcon fill="#DD9831" />
      </IconWrapper>
      <ContentWrapper>
        <TitleWrapper>
          <Title>About</Title>
          {isMe && (
            <EditButton onClick={() => setActiveEdit(isEdit ? null : "about")}>
              {isEdit
                ? <CloseIcon fill={defaultTheme.accent} />
                : <EditIcon fill={defaultTheme.accent} />}
            </EditButton>
          )}
        </TitleWrapper>
        {isEdit
          ? (
            <Formik
              initialValues={initialValues}
              onSubmit={async (values) => {
                await submitCallback(values);
                setActiveEdit(null);
              }}
            >
              {() => (
                <StyledForm>
                  <BaseInput
                    inputProps={{ name: "aboutMe", disabled: isLoading }}
                    width="100%"
                  />
                  <FormButton
                    isLoading={isLoading}
                    styleType="confirm"
                    label="Save and Close"
                    margin="20px 0 0 auto"
                  />
                </StyledForm>
              )}
            </Formik>
          )
          : <Content>{aboutMe || "Please fill About section"}</Content>}
      </ContentWrapper>
    </CardWrapper>
  );
};

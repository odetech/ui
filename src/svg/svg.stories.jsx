import React from 'react';
import { ChatBubbleSvg, TaskListSvg } from './index';
import styled from '@emotion/styled';

const SvgWrap = styled('div')`
  width: 60px;
  height: 60px;
  background: gray;
`;

const ChatBubbleWithBackground = () => (
  <SvgWrap>
    <ChatBubbleSvg />
  </SvgWrap>
)

const TaskListWithBackground = () => (
  <SvgWrap>
    <TaskListSvg />
  </SvgWrap>
)

export default {
  component: ChatBubbleWithBackground,
  title: 'SVG',
}

export const ChatBubble = () => <ChatBubbleWithBackground />;

export const TaskList = () => <TaskListWithBackground />;

import React from 'react';

export const TaskListSvg = () => (
  <svg id="task-list-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" aria-labelledby="title"
       aria-describedby="desc" role="img">
    <path data-name="layer2"
          fill="none" stroke="#ffffff" stroke-miterlimit="10" stroke-width="4" d="M22 14.9h40m-40 18h40m-40 18h40"
          stroke-linejoin="round" stroke-linecap="round"></path>
    <path data-name="layer1" fill="none" stroke="#ffffff" stroke-miterlimit="10"
          stroke-width="4" d="M2 14.9l4 4 4.8-7.9M2 32.9l4 4 4.8-7.9M2 50.9l4 4 4.8-8"
          stroke-linejoin="round" stroke-linecap="round"></path>
  </svg>
)

import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const WrapperWithDefaultStyles = ({ children }) => (
  <DefaultStylesWrapper>
    {children}
  </DefaultStylesWrapper>
);

WrapperWithDefaultStyles.propTypes = {
  children: PropTypes.node.isRequired,
};

export default WrapperWithDefaultStyles;

const DefaultStylesWrapper = styled('div')`
  font-family: 'Poppins', sans-serif !important;
  color: #3B3C3B;
  font-weight: 500;
  font-size: 15px;
  line-height: 22px;
  background: #FAFAFA;
  height: 100%;
  min-height: 100vh;
  max-height: 100%;

  @font-face {
    font-family: "Poppins";
    src: url("/src/stories/fonts/Poppins-Regular.ttf") format("truetype");
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
  }

  @font-face {
    font-family: "Poppins";
    src: url("/src/stories/fonts/Poppins-Bold.ttf") format("truetype");
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
  }

  @font-face {
    font-family: "Poppins";
    src: url("/src/stories/fonts/Poppins-Light.ttf") format("truetype");
    font-weight: 400;
    font-style: normal;
    font-stretch: normal;
  }
`;

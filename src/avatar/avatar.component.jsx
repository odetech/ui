import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { defaultTheme } from '../palette';
// import { UserDataProps } from '../props/UserProps';

// type AvatarWrapProps = {
//   size: number,
// }

// const AvatarWrap = styled('div')<AvatarWrapProps>`
//   width: ${(props) => props.size}px;
//   height: ${(props) => props.size}px;
//   border-radius: 50%;
//   overflow: hidden;
// `;

const AvatarWrap = styled('div')`
  width: ${(props) => props.size}px;
  height: ${(props) => props.size}px;
  border-radius: ${props => props.isSquare ? '6px' : '50%'};
  overflow: hidden;
  flex-shrink: 1;
`;

const AvatarImage = styled('img')`
  height: 100%;
  width: 100%;
  object-fit: cover;
`;

const AvatarPlaceholder = styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: ${({ fontSize }) => fontSize || '15px'};
  width: 100%;
  height: 100%;
  background-color: ${defaultTheme.accent};
  color: ${defaultTheme.background};
`;

// type AvatarProps = {
//   user: UserDataProps,
//   size?: number,
//   isSquare?: boolean,
// }

export const Avatar = ({
  user, size, isSquare, avatarPlaceholderFontSize,
}) => {
  if (!user?.profile) return null;

  const { firstName, lastName, avatar } = user.profile;

  return (
    <AvatarWrap size={size} isSquare={isSquare}>
      {avatar?.secureUrl ? (
        <AvatarImage
          src={avatar.secureUrl}
        />
      ) : (
        <AvatarPlaceholder
          fontSize={avatarPlaceholderFontSize}
        >
          {firstName.charAt(0)}
          {lastName.charAt(0)}
        </AvatarPlaceholder>
      )}
    </AvatarWrap>
  );
};

Avatar.propTypes = {
  isSquare: PropTypes.bool,
  size: PropTypes.number,
  avatarPlaceholderFontSize: PropTypes.string,
  user: PropTypes.shape({}),
};

Avatar.defaultProps = {
  isSquare: false,
  size: 44,
  user: null,
  avatarPlaceholderFontSize: '',
};

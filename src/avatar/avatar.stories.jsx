import React from 'react';
import { Avatar } from './avatar.component';
import { user } from '../mocks/user';

export default {
  component: Avatar,
  title: 'Avatar',
}

const Template = (args) => <Avatar {...args} />;

export const AvatarPlaceholder = Template.bind({});
AvatarPlaceholder.args = {
  user: {
    ...user,
    profile: {
      ...user.profile,
      avatar: null,
    }
  },
};

export const AvatarWithImage = Template.bind({});
AvatarWithImage.args = {
  user,
};

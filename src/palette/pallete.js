export const defaultTheme = {
  accent: '#F7942A',
  accentHover: '#EFA657',
  accentFocus: '#CD791F',
  disabled: '#F0F0F0',

  green: '#4BC133',
  greenHover: '#84BE78',
  greenFocus: '#3A9527',

  blue: '#3B7EE1',
  blueHover: '#6B96D7',
  blueFocus: '#2A5AA3',

  pink: '#E54367',
  pinkHover: '#E5708A',
  pinkFocus: '#AB314C',

  red: '#F33822',
  redHover: '#F2796A',
  redFocus: '#C92B18',

  purple: '#9131DD',
  purpleHover: '#AA6DDB',
  purpleFocus: '#7728B5',

  text: '#3B3C3B',
  label: '#9C9C9C',
  border: '#E5E5E5',

  background: '#FFFFFF',
  backgroundHover: '#FEF4E9',
  backgroundFocus: '#FDDFC0'
};


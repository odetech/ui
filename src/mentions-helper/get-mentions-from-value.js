export const getMentionsFromValue = (value, usersIds) => {
  const parser = new DOMParser();
  const htmlDoc = parser.parseFromString(value, 'text/html');
  const mentionUserIdList = [];
  for (let i = 0; i < htmlDoc.getElementsByTagName("a").length; i++) {
    const id = htmlDoc.getElementsByTagName("a").item(i).getAttribute('href').split('?mention=@')[1];
    if (id && id !== 'channel') {
      mentionUserIdList.push(id);
    }
    if (id && id === 'channel' && usersIds?.length) {
      mentionUserIdList.push(...usersIds);
    }
  }

  const setList = new Set(mentionUserIdList);

  return [...setList];
}

// TODO not ready yet
export type UserDataProps = {
  createdAt: string,
  emails: { address: string, verified: boolean }[],
  profile: {
    aboutMe: string,
    avatar: null | {
      secureUrl: string,
      _id: string,
    },
    createdBy: string,
    createdFrom: string,
    firstName: string,
    lastName: string,
    onboardings: null,
    roles: {
      _id: string,
      isStaff: boolean,
    } | null,
    skillsets: null,
    tagline: string,
  },
  username: string,
  _id: string,
};

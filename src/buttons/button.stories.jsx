import React from 'react';
import { Button } from './button.component';

export default {
  component: Button,
  title: 'Buttons',
}
const Icon = () => (
  <svg viewBox='0 0 24 24'>
    <path
      d="M9.29289 7.70711L12.5858 11L2 11L2 13L12.5858 13L9.29289 16.2929L10.7071 17.7071L16.4142 12L10.7071
      6.29289L9.29289 7.70711ZM20 2L16 2L16 4L20 4L20 20L16 20L16 22L20 22C21.1046 22 22 21.1046 22 20L22 4C22
      2.89543 21.1046 2 20 2Z"
    />
  </svg>
);

const Template = (args) => <Button {...args}>Invite To Network</Button>;
const TemplateLeftIcon = (args) => <Button {...args}><Icon /><span>Invite To Network</span></Button>;
const TemplateRightIcon = (args) => <Button {...args}><span>Invite To Network</span><Icon /></Button>;

export const Default = Template.bind({});
Default.args = {};

export const Text = Template.bind({});
Text.args = {
  view: 'text',
};

export const DefaultLeftIcon = TemplateLeftIcon.bind({});
DefaultLeftIcon.args = {};

export const TextLeftIcon = TemplateLeftIcon.bind({});
TextLeftIcon.args = {
  view: 'text',
};

export const DefaultRightIcon = TemplateRightIcon.bind({});
DefaultRightIcon.args = {};

export const TextRightIcon = TemplateRightIcon.bind({});
TextRightIcon.args = {
  view: 'text',
};


import React from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import { defaultTheme } from '../palette';

const StyledButton = styled('button')`
  transition: 0.3s;
  cursor: pointer;
  outline: none;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ view, color }) => (view === 'default' ? defaultTheme[color] : 'transparent')} none;
  border: none;
  border-radius: 8px;
  color: ${({ view, color }) => (view === 'default' ? defaultTheme.background : defaultTheme[color])};
  line-height: 1;
  font-weight: 500;

  // Size of the Button (small(sm), large(lg), medium(default/md))
  ${({ size }) => {
  switch (size) {
    case 'xs':
      return `
        padding: 0 12px;
        height: 28px;
        font-size: 13px;

        svg {
          height: 16px;
          width: 16px;
        }
      `;
    case 'sm':
      return `
        padding: 0 16px;
        height: 36px;
        font-size: 15px;

        svg {
          height: 16px;
          width: 16px;
        }
      `;
    case 'lg':
      return `
        padding: 0 36px;
        height: 70px;
        font-size: 20px;
        font-weight: 600;

        svg {
          height: 24px;
          width: 24px;
        }
      `;
    default:
      return `
        padding: 0 24px;
        height: 44px;
        font-size: 15px;

        svg {
          height: 18px;
          width: 18px;
        }
      `;
  }
}};

  &:hover {
    background: ${({ view, color }) => (view === 'default'
      ? defaultTheme[`${color}Hover`] : defaultTheme.backgroundHover)};
  }

  &:focus,
  &:active {
    outline: none;
    background: ${({ view, color }) => (view === 'default'
  ? defaultTheme[`${color}Focus`] : defaultTheme.backgroundFocus)};
  }


  svg {
    fill: ${({ view, color }) => (view === 'default' ? defaultTheme.background : defaultTheme[color])};

    &:first-child:not(:last-child) {
      margin-right: 12px;
    }

    &:last-child:not(:first-child) {
      margin-left: 12px;
    }
  }

  &:disabled {
    cursor: not-allowed;
    background: ${defaultTheme.disabled};
    color: ${defaultTheme.label};

    svg {
      fill: ${defaultTheme.label};
    }
  }
`;

// type ButtonProps = {
//   id?: string;
//   onClick?: (e: MouseEvent<HTMLButtonElement>) => void;
//   disabled?: boolean;
//   type?: 'button' | 'reset' | 'submit';
//   children: ReactNode;
//   size?: 'lg' | 'sm' | 'md' | 'xs';
//   view?: 'default' | 'text';
//   color?: string | 'orange' | 'green' | 'red' | 'pink' | 'yellow' | 'blue' | 'purple' | 'black';
// };

export const Button = ({
  type, size, view, disabled, color, children, ...props
}) => (
  <StyledButton
    type={type}
    disabled={disabled}
    size={size}
    view={view}
    color={color}
    {...props}
  >
    {children}
  </StyledButton>
);

export default Button;

Button.propTypes = {
  disabled: PropTypes.bool,
  children: PropTypes.element.isRequired,
  size: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
  view: PropTypes.oneOf(['default', 'text']),
  color: PropTypes.oneOf(['accent', 'green', 'red', 'pink', 'blue', 'purple']),
};

Button.defaultProps = {
  disabled: false,
  size: 'md',
  view: 'default',
  color: 'accent',
};

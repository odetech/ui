module.exports = {
    'env': {
        'test': {
            'presets': [
                "@babel/preset-react",
                [
                    '@babel/env',
                    {'targets': {'node': 'current'}}
                ]
            ]
        }
    }
}
